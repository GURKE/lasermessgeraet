#include "menu.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Arduino.h>
#include "Control.h"
#include <stdint.h>

bool (*_move_function)(uint32_t* x, const uint16_t eeprom_x, uint16_t* d, const uint16_t eeprom_d, Stepper_Part sp, const __FlashStringHelper* hint);

/* Menüstruktur
 * Hauptmenü:
 *  Verfahren
 *  Dateibrowser
 *  Einstellungen
 *    Referenz setzen
 *    D-unten
 *    Stepper
 *     Max Geschwindigkeit X
 *     Max Geschwindigkeit D
 *     Beschleunigung X
 *     Beschleunigung D
 *     Scaling Stepper X
 *     Scaling Stepper D
 *     Neustart
 *    Blockhöhe
 *  Aktuelle Daten
 *    Allgemein
 *    EEPROM
 *    Referenz
 *  Automessung
 *    Inkrement
 *      X
 *      D Grob
 *      D Klein
 *    Streben
 *      Erste
 *      Letzte
 *      Höhe
 *    Dateiname
 *    Dezimaltrennzeichen
 *    Grenzen setzen
 *      Start
 *      X Ende
 *      D Unten
 *    Spiegelmodus
 *    Start Automessung
 *  Tests
 *    X-Achse
 *    D-Achse
 *    Länge
 *  Neustart
 */

 // Menü: Aktuelle Daten
Menu::Element aktuelledaten_allgemein = Menu::Element("Allgemein", Menu::Type::Function);
Menu::Element aktuelledaten_eeprom = Menu::Element("EEPROM", Menu::Type::Function);
Menu::Element aktuelledaten_referenz = Menu::Element("Referenzdaten", Menu::Type::Function);
Menu::Element aktuelledaten_stepper = Menu::Element("Stepper", Menu::Type::Function);
Menu m_aktuelledaten = Menu(4, (Menu::Element * []) { &aktuelledaten_allgemein, & aktuelledaten_eeprom, & aktuelledaten_referenz, & aktuelledaten_stepper });
const uint8_t aktuelledaten_index = 3;

// Menü: Allgemeine Einstellungen
// Menü: Allgemeine Einstellungen - Stepper
Menu::Element steppers_speedx = Menu::Element("Geschwindigkeit X", Menu::Type::Variable);
Menu::Element steppers_speedd = Menu::Element("Geschwindigkeit D", Menu::Type::Variable);
Menu::Element steppers_accelerationx = Menu::Element("Beschleunigung X", Menu::Type::Variable);
Menu::Element steppers_accelerationd = Menu::Element("Beschleunigung D", Menu::Type::Variable);
Menu::Element steppers_scalingx = Menu::Element("Skalierung X", Menu::Type::Variable);
Menu::Element steppers_scalingd = Menu::Element("Skalierung D", Menu::Type::Variable);
Menu::Element steppers_hint = Menu::Element("Neustart (notwendig!)", Menu::Type::Function);
Menu m_settings_steppers = Menu(7, (Menu::Element * []) { &steppers_speedx, & steppers_speedd, & steppers_accelerationx, & steppers_accelerationd, & steppers_scalingx, & steppers_scalingd, & steppers_hint });
const uint8_t settings_steppers_index = 2;

// Menü: Allgemeine Einstellung
Menu::Element settings_referenz = Menu::Element("Referenz", Menu::Type::Function);
Menu::Element settings_d_low = Menu::Element("D-Unten", Menu::Type::Variable);
Menu::Element settings_steppers = Menu::Element("Stepper", &m_settings_steppers);
Menu::Element settings_block_height = Menu::Element("Blockhöhe", Menu::Type::Variable);
Menu::Element settings_minimal_measurement = Menu::Element("Mindestmesswert", Menu::Type::Variable);
Menu m_settings = Menu(5, (Menu::Element * []) { &settings_referenz, & settings_d_low, & settings_steppers, & settings_block_height, & settings_minimal_measurement });
const uint8_t settings_index = 2;

// Menü: Automatische Messung
// Menü: Automatische Messung - Inkrement
Menu::Element automaticmeasure_inkrement_xgrob = Menu::Element("X Grob", Menu::Type::Variable);
Menu::Element automaticmeasure_inkrement_xfein = Menu::Element("X Fein", Menu::Type::Variable);
Menu::Element automaticmeasure_inkrement_xfein_anz = Menu::Element("Anzahl Fein X", Menu::Type::Variable);
Menu::Element automaticmeasure_inkrement_dgrob = Menu::Element("D Grob", Menu::Type::Variable);
Menu::Element automaticmeasure_inkrement_dfein = Menu::Element("D Fein", Menu::Type::Variable);
Menu::Element automaticmeasure_inkrement_dfein_anz = Menu::Element("Anzahl Fein D", Menu::Type::Variable);
Menu m_inkrement = Menu(6, (Menu::Element * []) { &automaticmeasure_inkrement_xgrob, & automaticmeasure_inkrement_xfein, & automaticmeasure_inkrement_xfein_anz, & automaticmeasure_inkrement_dgrob, & automaticmeasure_inkrement_dfein, & automaticmeasure_inkrement_dfein_anz });
const uint8_t inkrement_index = 0;

// Menü: Automatische Messung - Streben
Menu::Element automaticmeasure_streben_erste = Menu::Element("Erste", Menu::Type::Variable);
Menu::Element automaticmeasure_streben_letzte = Menu::Element("Letzte", Menu::Type::Variable);
Menu::Element automaticmeasure_streben_hoehe = Menu::Element("Höhe", Menu::Type::Function);
Menu m_streben = Menu(3, (Menu::Element * []) { &automaticmeasure_streben_erste, & automaticmeasure_streben_letzte, & automaticmeasure_streben_hoehe });
const uint8_t streben_index = 1;

// Menü: Automatische Messung - Grenzen
Menu::Element automaticmeasure_grenzen_start = Menu::Element("Start", Menu::Type::Function);
Menu::Element automaticmeasure_grenzen_xende = Menu::Element("X Ende", Menu::Type::Variable);
Menu::Element automaticmeasure_grenzen_dunten = Menu::Element("D Unten", Menu::Type::Variable);
Menu m_grenzen = Menu(3, (Menu::Element * []) { &automaticmeasure_grenzen_start, & automaticmeasure_grenzen_xende, & automaticmeasure_grenzen_dunten });
const uint8_t border_index = 4;

// Menü: Automatische Messung
Menu::Element automaticmeasure_inkrement = Menu::Element("Inkrement", &m_inkrement);
Menu::Element automaticmeasure_streben = Menu::Element("Streben", &m_streben);
Menu::Element automaticmeasure_filename = Menu::Element("Dateiname", Menu::Type::Function);
Menu::Element automaticmeasure_autoname = Menu::Element("Dezimaltrennzeichen", Menu::Type::Variable);
Menu::Element automaticmeasure_grenzen = Menu::Element("Grenzen setzen", &m_grenzen);
Menu::Element automaticmeasure_mirror_mode = Menu::Element("Spiegelmodus", Menu::Type::Variable);
Menu::Element automaticmeasure_messung_starten = Menu::Element("Start Automessung", Menu::Type::Function);
Menu m_automaticmeasure = Menu(7, (Menu::Element * []) { &automaticmeasure_inkrement, & automaticmeasure_streben, & automaticmeasure_filename, & automaticmeasure_autoname, & automaticmeasure_grenzen, & automaticmeasure_mirror_mode, & automaticmeasure_messung_starten });
const uint8_t automaticmeasure_index = 4;

// Menü: Tests
Menu::Element tests_X_Achse = Menu::Element("X-Achse", Menu::Type::Function);
Menu::Element tests_D_Achse = Menu::Element("D-Achse", Menu::Type::Function);
Menu::Element tests_length = Menu::Element("Länge", Menu::Type::Function);
Menu::Element tests_praezision = Menu::Element("Präzision", Menu::Type::Function);
Menu m_tests = Menu(4, (Menu::Element * []) { &tests_X_Achse, & tests_D_Achse, & tests_length, & tests_praezision });
const uint8_t tests_index = 5;

// Menü: Haupt
Menu::Element verfahren = Menu::Element("Verfahren", Menu::Type::Function);
Menu::Element dateibrowser = Menu::Element("Dateibrowser", Menu::Type::Function);
Menu::Element setreference = Menu::Element("Einstellungen", &m_settings);
Menu::Element aktuelledaten = Menu::Element("Aktuelle Daten", &m_aktuelledaten);
Menu::Element automaticmeasure = Menu::Element("Automessung", &m_automaticmeasure);
Menu::Element tests = Menu::Element("Tests", &m_tests);
Menu::Element reboot = Menu::Element("Neustart", Menu::Type::Function);
Menu main_menu = Menu(7, (Menu::Element * []) { &verfahren, & dateibrowser, & setreference, & aktuelledaten, & automaticmeasure, & tests, & reboot });


Menu::Menu() {

}

Menu::Menu(uint8_t _am_elements, Menu::Element* _elements[]) : am_elements(_am_elements) {
    this->elements = (Menu::Element**)malloc(am_elements * sizeof(Menu::Element*));
    for (uint8_t i = 0; i < am_elements; i++) {
        this->elements[i] = _elements[i];
        this->elements[i]->parent = this;
    }
}

Menu::Element::Element(const char* _title) : title(_title), menu(nullptr), fun(nullptr) {
    if (strlen(_title) > 17) {
        printf("Achtung Titel: '"); Serial.print(_title); Serial.print("' ist zu lang ("); Serial.print(strlen((const char*)_title)); Serial.print(" Zeichen)\n");
    }
}

Menu::Element::Element(const char* _title, Menu* _menu) : title(_title), type(Type::Menu), menu(_menu), fun(nullptr) {
    menu->parent = this;
    if (strlen(_title) > 17) {
        printf("Achtung Titel: '"); Serial.print(_title); Serial.print("' ist zu lang ("); Serial.print(strlen((const char*)_title)); Serial.print(" Zeichen)\n");
    }
}

Menu::Element::Element(const char* _title, bool(*_fun)(bool serial)) : title(_title), type(Type::Function), menu(nullptr), fun(_fun) {
    if (strlen(_title) > 17) {
        printf("Achtung Titel: '"); Serial.print(_title); Serial.print("' ist zu lang ("); Serial.print(strlen((const char*)_title)); Serial.print(" Zeichen)\n");
    }
}

Menu::Element::Element(const char* _title, Type _type) : title(_title), type(_type), menu(nullptr), fun(nullptr) {

}

bool Menu::Element::enter(Menu** cur_menu, bool serial) {
    switch (this->type) {
    case Type::Function: {
        if (this->fun == nullptr) {
            Serial.print("Function not defined yet!\n");
        } else
            return this->fun(serial);
        break;
    }
    case Type::Menu: {
        if (this->menu == nullptr) {
            Serial.print("Function not defined yet!\n");
        } else
            *cur_menu = this->menu;
        return true;
    }
    case Type::Variable: {
        this->variable->Change();
        return serial;
    }
    default: {
        break;
    }
    }

    return serial;
}

bool Menu::Element::leave(Menu** cur_menu, bool serial) {
    if ((*cur_menu)->parent == nullptr) {
        Serial.print("Bereits im Hauptmenu!\n");
    } else {
        *cur_menu = (*cur_menu)->parent->parent;
    }
    return true;
}

Variable::Variable(Type _type, const __FlashStringHelper* _description, void* _variable, const uint16_t _eeprom) : type(_type), description(_description), variable(_variable), eeprom(_eeprom) {}


void Variable::Change() {
    Serial.println(this->description);
    if (this->type == Type::UINT16) {
        uint16_t& val = *((uint16_t*)this->variable);
        Serial.print(F("Aktueller Wert: ")); Serial.println(val);
        if (get_value(F("Wert eingeben"), F("Wert eingeben"), &val)) {
            if (message(F("Wert im EEPROM speichern?"), F("Wert im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK) == MESSAGE_RESPONSE_YES)
                EEPROM.put(this->eeprom, val);
        }
    } else if (this->type == Type::UINT32) {
        Serial.println("Not implemented yet!");
    } else if (this->type == Type::BOOL) {
        bool& bval = *((bool*)this->variable);
        uint16_t ival = bval;
        if (get_value(F("Wert eingeben"), F("Wert eingeben: 0 - false, 1 - true"), &ival)) {
            bval = ival;
            if (message(F("Wert im EEPROM speichern?"), F("Wert im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK) == MESSAGE_RESPONSE_YES)
                EEPROM.put(this->eeprom, bval);
        }
    } else if (this->type == Type::SELECTION) {
        T_Select* s = (T_Select*)this->variable;
        for (uint8_t i = 0; i < s->amount; i++) {
            Serial.print(" "); Serial.print(i + 1); Serial.print(F(" - ")); Serial.print(s->options[i]);
            if (*s->value == i)
                Serial.print(F(" <-"));

            Serial.print(F("\n"));
        }
        Serial.print(F(" q - Ohne Änderung zurück\n"));

        while (true) {
            while (!Serial.available());
            char c = Serial.read();
            if (c > '0' && c < s->amount + '1') {
                *s->value = c - '1';

                if (message(F("Wert im EEPROM speichern?"), F("Wert im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK) == MESSAGE_RESPONSE_YES)
                    EEPROM.put(this->eeprom, (uint8_t)*s->value);

                break;
            } else if (c == 'q')
                return;
            else if (c == '\n')
                ;
            else
                Serial.print("Falsche Eingabe!\n");
        }
    } else if (this->type == Type::POSITION) {
        Serial.println(F("Position setzen"));
        T_Position* s = (T_Position*)this->variable;
        _move_function(s->X, s->eeprom_X, s->D, s->eeprom_D, s->part, this->description);
    } else if (this->type == Type::FLOAT) {
        float& fval = *((float*)this->variable);
        Serial.print(F("Aktueller Wert: ")); Serial.println(fval, 6);

        while (Serial.available())
            Serial.read();
        Serial.println(F("Kommazahl eingeben:"));

        float nval = 0;

        while (1) {
            static float exp = 1;
            char c = Serial.read();
            if (c == -1)
                continue;
            else if (c == '\n' || c == '\0' || c == '\r')
                break;
            else if (c == '.' || c == ',')
                exp /= 10;
            else if (c == 'q')
                return;
            else if (c >= '0' && c <= '9') {
                if (exp == 1) // Vorkommazahl
                    nval = 10 * nval + (c - '0');
                else {
                    nval += (c - '0') * exp;
                    exp /= 10;
                }
            }
        }

        Serial.print(F("Neuer Wert: ")); Serial.println(nval, 6);
        while (Serial.available())
            Serial.read();
        Serial.println(F("Wert setzen?\n(n)< Nein Ja >(j)"));
        while (1) {
            char c = Serial.read();
            if (c == 'j' || c == 'y') {
                Serial.println(F("Wert gespeichert!"));
                fval = nval;
                if (message(F("Wert im EEPROM speichern?"), F("Wert im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK) == MESSAGE_RESPONSE_YES)
                    EEPROM.put<float>(this->eeprom, fval);
                break;
            } else if (c == 'n' || c == 'q')
                return;
        }
    } else
        Serial.println(F("Variable type not defined!"));
}

Variable::T_Select::T_Select(uint8_t* _value, uint8_t _amount, const char** _options) : value(_value), amount(_amount), options(_options) {}

Variable::T_Position::T_Position(Stepper_Part _part, uint32_t* _X, const uint16_t _eeprom_X, uint16_t* _D, const uint16_t _eeprom_D) : part(_part), X(_X), eeprom_X(_eeprom_X), D(_D), eeprom_D(_eeprom_D) {}