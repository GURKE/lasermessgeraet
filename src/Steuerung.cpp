// NEXT EEPROM-ADRESS: Siehe Tabelle https://wiki.marmeladenkekse.de/index.php?title=Lasermesseinrichtung#Bootsystem
#include "Control.h"
#include <EEPROM.h>
#include <AccelStepper.h> // Bibliotheksverwaltung / https://www.airspayce.com/mikem/arduino/AccelStepper/
#include <AFMotor.h> // https://github.com/adafruit/Adafruit-Motor-Shield-library/blob/master/AFMotor.h
#include <SPI.h>
#include "menu.h"
#include <WString.h>
#include <avr/wdt.h>
#include "Laser.h"
#include "Filebrowser.h"
#include "Steuerung.h"

// #define OFFLINE_MODE // Auskommentieren, um die Stepper zu aktivieren

/*  Schrittmotoren  *******************************************************************************************************************************************************************************************/

#define StepperPulsePINX 29
#define StepperDirectionPINX 27
#define StepperPulsePIND 25
#define StepperDirectionPIND 23

AccelStepper stepperX(AccelStepper::DRIVER, StepperPulsePINX, StepperDirectionPINX);
AccelStepper stepperD(AccelStepper::DRIVER, StepperPulsePIND, StepperDirectionPIND);

const uint16_t eeprom_scalingx = 0x47;
float ScalingX = 0.039895; // Skalierungsfaktor von Schritt zu mm: Reibraddurchmesser=15mm, Schritte/U=200, Modus=Achtelschritte => 0,029446875mm/Schritt
const uint16_t eeprom_scalingd = 0x4B;
float ScalingD = 0.039895; // Skalierungsfaktor von Schritt zu mm: Reibraddurchmesser=15mm, Schritte/U=200, Modus=Achtelschritte => 0,029446875mm/Schritt

#define SCALEXMTS(x) ((x) / ScalingX) // Umrechnungsfunktion von mm zu steps
#define SCALEDMTS(d) ((d) / ScalingD) // Umrechnungsfunktion von mm zu steps
#define SCALEXSTM(x) ((x) * ScalingX) // Umrechnungsfunktion von steps zu mm
#define SCALEDSTM(d) ((d) * ScalingD) // Umrechnungsfunktion von steps zu mm

const uint16_t eeprom_stepper_maxspeed_x = 0x33;
uint16_t stepper_maxspeed_x = 0; // Maximale Geschwindigkeit der X-Achse in mm/s (50)
const uint16_t eeprom_stepper_maxspeed_d = 0x35;
uint16_t stepper_maxspeed_d = 0; // Maximale Geschwindigkeit der D-Achse in mm/s (100)
const uint16_t eeprom_stepper_acceleration_x = 0x37;
uint16_t stepper_acceleration_x = 0; // Maximale Beschleunigung der X-Achse in mm/s^2 (100)
const uint16_t eeprom_stepper_acceleration_d = 0x39;
uint16_t stepper_acceleration_d = 0; // Maximale Beschleunigung der D-Achse in mm/s^2 (100)

const uint16_t eeprom_block_height = 0x18;
uint16_t block_height; // Höhe vom Bootsauflageblock

/* Die Schrittmotoren-Referenz: Punkt senkrecht unter Nullpunkt auf dem Boden. Dieser wird gewählt, da bei liegendem Boot der Nullpunkt nicht mehr angefahren werden kann. */
uint32_t Ref_PosXStep = 0; // X-Position am Referenzpunkt in Steps
const uint32_t eeprom_Ref_PosXStep = 0x29;
uint16_t Ref_PosMMM = 0; // M-Wert am Referenzpunkt in mm, wird beim Referenzieren am Referenzpunkt gesetzt
const uint16_t eeprom_Ref_PosMMM = 0x2D;
uint16_t Ref_PosDStep = 0; // M-Wert am Referenzpunkt in Steps, wird beim Referenzieren am Referenzpunkt gesetzt
const uint16_t eeprom_Ref_PosDStep = 0x2F;

uint16_t Ref_Height = 0; // Höhe Boden - Nullpunkt in mm
const uint16_t eeprom_Ref_Height = 0x31;

// Spiegelmodus für Nutzung des Lasers auf anderer Seite
const uint16_t eeprom_AM_mirrormode = 0x46;
enum Mirrormode : uint8_t { Normal, Gantry_rotated, D_Axis_mirrored } AM_mirrormode;

// Minimaler Messwert vom Laser der als plausibel erachtet wird in mm
const uint16_t eeprom_am_minimal_measurement = 0x10;
uint16_t am_minimal_measurement;

// Coordinates
#define SQRT2 1.41421356
#define GETPOSXSTEP() (stepperX.currentPosition()) // X-Pos in step
#define GETPOSXMM() ((int16_t)(SCALEXSTM(GETPOSXSTEP()))) // X-Pos in mm
#define GETPOSDSTEP() (stepperD.currentPosition()) // D-Pos in step
#define GETPOSDMM() ((int16_t)(SCALEDSTM(GETPOSDSTEP()))) // D-Pos in mm
#define GETPOSMMM(value) manual_measure_int(value) // M-Pos in mm
static int16_t GETPOSRMM(int32_t x) {
  if (AM_mirrormode == Mirrormode::Gantry_rotated)
    return (int16_t)SCALEXSTM(x - (int32_t)Ref_PosXStep);
  else
    return (int16_t)SCALEXSTM((int32_t)Ref_PosXStep - x);
}
static int16_t GETPOSRMM() {
  return GETPOSRMM(GETPOSXSTEP());
}
static int16_t GETPOSSMM(uint16_t m) {
  if (AM_mirrormode == Mirrormode::Normal)
    return (int16_t)((Ref_PosMMM - m + SCALEDSTM(GETPOSDSTEP() - Ref_PosDStep)) / SQRT2);
  else
    return -(int16_t)((Ref_PosMMM - m + SCALEDSTM(GETPOSDSTEP() - Ref_PosDStep)) / SQRT2);
}
static float GETPOSSMMF(uint16_t m) {
  if (AM_mirrormode == Mirrormode::Normal)
    return (Ref_PosMMM - m + SCALEDSTM(GETPOSDSTEP() - Ref_PosDStep)) * 1.0 / SQRT2;
  else
    return -(Ref_PosMMM - m + SCALEDSTM(GETPOSDSTEP() - Ref_PosDStep)) * 1.0 / SQRT2;
}
#define GETPOSTMM(m) ((int16_t)((Ref_PosMMM-m+SCALEDSTM(Ref_PosDStep-GETPOSDSTEP()))/SQRT2-Ref_Height))
#define GETPOSTMMF(m) ((float)((Ref_PosMMM-m+SCALEDSTM(Ref_PosDStep-GETPOSDSTEP()))*1.0/SQRT2-Ref_Height))
#define PRINT_RST(m) Serial.print(F("Measurement: ")); Serial.print(m); Serial.print(F(" mm, X: ")); Serial.print(GETPOSXMM()); Serial.print(F(" mm, D: ")); Serial.print(GETPOSDMM()); Serial.print(F(" mm, R(→): ")); Serial.print(GETPOSRMM()); Serial.print(F(" mm, S(☉): ")); Serial.print(GETPOSSMM(m)); Serial.print(F(" mm, T(↑): ")); Serial.print(GETPOSTMM(m)); Serial.println(F(" mm"));
#define PRINT_VARIABLE(v, s) Serial.print(s); Serial.print(F(" = ")); Serial.print(v); Serial.print(F(" steps = ")); Serial.print(s == 'x' ? SCALEXSTM(v) : SCALEDSTM(v)); Serial.println(F(" mm"));

// Grenzpositionen (d_max ist der Endanschlag und somit steps=0)
const uint16_t eeprom_d_low = 0x08; // TODO Not used!?
// Unterer "imaginärer" Anschlag (in steps)
uint16_t d_low;

// Endanschlag der D-Achse
#define ENDSWITCHD 20 // Pin des Endanschlags der D-Achse
volatile bool EndSwitchDTriggered = false;
#define ENDSWITCHDTRIGGERED() (digitalRead(ENDSWITCHD) == HIGH)

#define ENDSWITCHX 21 // Pin des Endanschlages der X-Achse
volatile bool EndSwitchXTriggered = false;
#define ENDSWITCHXTRIGGERED() (digitalRead(ENDSWITCHX) == LOW)

/*  Automatische Messung  ******************************************************************************************************************************************************************************************/
uint32_t AM_Strut_first = 0, AM_Strut_last = 0; // in steps
const uint16_t eeprom_AM_Strut_first = 0x1F, eeprom_AM_Strut_last = 0x23;
int16_t AM_MachineZ;
const uint16_t eeprom_AM_MachineZ = 0x41;

// Inkremente für D- und X-Achse in mm
const uint16_t eeprom_AM_Inkrement_D_G = 0x27;
uint16_t AM_Inkrement_D_G = 0;
const uint16_t eeprom_AM_Inkrement_D_K = 0x0C;
uint16_t AM_Inkrement_D_K = 0;
const uint16_t eeprom_AM_Inkrement_X_G = 0x0E;
uint16_t AM_Inkrement_X_G = 0;
const uint16_t eeprom_AM_Inkrement_X_K = 0x1A;
uint16_t AM_Inkrement_X_K = 0;

// Objekt-Grenzen
const uint16_t eeprom_AM_X_Start_S = 0x3B;
uint32_t AM_X_Start_S = 0;
const uint16_t eeprom_AM_D_Start_S = 0x00;
uint16_t AM_D_Start_S = 0;
const uint16_t eeprom_AM_X_Max_S = 0x02;
uint32_t AM_X_Max_S = 0;
const uint16_t eeprom_AM_D_Bot_S = 0x06;
uint16_t AM_D_Bot_S = 0;

// Anzahl kleiner Schritte
const uint16_t eeprom_AM_Amount_Smallloops_D = 0x44;
uint16_t AM_Amount_Smallloops_D = 0;
const uint16_t eeprom_AM_Amount_Smallloops_X = 0x1C;
uint16_t AM_Amount_Smallloops_X = 0;

const uint16_t eeprom_AM_decimalpoint = 0x1E;
enum Decimalpoint : uint8_t { Comma, Point } AM_decimalpoint;
char AM_value_separator;
char AM_value_decimalpoint;

// Maximale Anzahl an Chars eines gespeicherten Messpunktes
const uint16_t POINT_SIZE = 1 + 5 + 1 + 6 + 1 + 6 + 1;

/*  Stop-Taster ******************************************************************************************************************************************************************************************/
#define STOPBUTTONPIN A15
#define STOPBUTTONTRIGGERED (digitalRead(STOPBUTTONPIN) == LOW)

/*  Nothalt   ******************************************************************************************************************************************************************************************/
#define EMERGENCYSTOPPIN 19
volatile bool EmergencyStopTriggered = false;
#ifdef OFFLINE_MODE
#define EMERGENCYSTOPTRIGGERED 0
#else
#define EMERGENCYSTOPTRIGGERED (digitalRead(EMERGENCYSTOPPIN) == LOW)
#endif

#define STATE_NORMAL 0
#define STATE_CLICKED 1
#define STATE_PAUSE 2
#define STATE_STOP 3
volatile uint8_t state = STATE_NORMAL;

/* Allgemein ******************************************************************************************************************************************************************************************/
const uint16_t EEPROM_SIZE = 4096;
const uint8_t MENU_POSITIONS = 9;
const char* automaticmeasure_mirrormode_options[3] = { "Normaler Modus", "Gantrydrehung", "D-Achsen-Spiegelung" };
const char* automaticmeasure_decimalpoint_options[2] = { "Komma", "Punkt" };
bool Sd_inited = false;

/*  Funktionsbeginn ******************************************************************************************************************************************************************************************/

void setup() {
  // Initialisieren des LCD
  lcd.begin(16, 2);

  Serial.begin(57600); // Initialisieren der Computer-Schnittstelle
  Serial2.begin(115200); // Initialisieren der Messgeräte-Schnittstelle
  Serial.readString(); // Ggf. anliegende veraltete Daten löschen

  // Infotext printen
  message(F("Messeinrichtung v0.14 22.05.2021"), F("************************************\n* Messeinrichtung v0.14 22.05.2021 *\n************************************\n"), MESSAGE_TYPE_INFO);
  Serial.println(SERIALINFOTEXT);

  // Eingänge initialisieren
  pinMode(ExternalSwitchPin, INPUT_PULLUP);

  // Endanschlagsschalter und Interrupt für D-Achse initialisieren
  pinMode(ENDSWITCHD, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ENDSWITCHD), isr_EndSwitchD, RISING);
  if (ENDSWITCHDTRIGGERED())
    EndSwitchDTriggered = true;

  // Endanschlagsschalter und Interrupt für X-Achse initialisieren
  pinMode(ENDSWITCHX, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ENDSWITCHX), isr_EndSwitchX, FALLING);
  if (ENDSWITCHXTRIGGERED())
    EndSwitchXTriggered = true;

  // Notstop-Interrupt initialisieren
  pinMode(EMERGENCYSTOPPIN, INPUT);
#ifndef OFFLINE_MODE
  attachInterrupt(digitalPinToInterrupt(EMERGENCYSTOPPIN), isr_EmergencyStop, FALLING);
#endif

  // Externen Taster-Interrupt initialisieren
  pinMode(A15, INPUT_PULLUP);
  *digitalPinToPCMSK(A15) |= bit(digitalPinToPCMSKbit(A15));  // enable pin
  PCIFR |= bit(digitalPinToPCICRbit(A15)); // clear any outstanding interrupt
  PCICR |= bit(digitalPinToPCICRbit(A15)); // enable interrupt for the group 

  // Eingabestartwert setzen
  last_read_char = 0;
  _SpecialCommand = SpecialCommand;

  delay(1500);

  // Initialisieren der Schrittmotoren
  // Laden von Einstellungen
  EEPROM.get<float>(eeprom_scalingx, ScalingX);
  EEPROM.get<float>(eeprom_scalingd, ScalingD);

  EEPROM.get(eeprom_d_low, d_low);
  EEPROM.get(eeprom_block_height, block_height);
  EEPROM.get(eeprom_stepper_maxspeed_x, stepper_maxspeed_x);
  EEPROM.get(eeprom_stepper_maxspeed_d, stepper_maxspeed_d);
  EEPROM.get(eeprom_stepper_acceleration_x, stepper_acceleration_x);
  EEPROM.get(eeprom_stepper_acceleration_d, stepper_acceleration_d);

  stepperX.setMaxSpeed(SCALEXMTS(stepper_maxspeed_x));
  stepperX.setAcceleration(SCALEXMTS(stepper_acceleration_x));
  stepperD.setMaxSpeed(SCALEDMTS(stepper_maxspeed_d));
  stepperD.setAcceleration(SCALEDMTS(stepper_acceleration_d));


  if (EMERGENCYSTOPTRIGGERED)
    message(F("24V aktivieren!"), F("Stromversorgung (24V) aktivieren"), MESSAGE_TYPE_INFO);
  while (EMERGENCYSTOPTRIGGERED)
    delay(250); // Entprellen
  EmergencyStopTriggered = false;

  message(F("Referenziere Achsen..."), F("Referenziere Achsen..."), MESSAGE_TYPE_INFO);
  referenceAxis(); // Position von D am Limit Switch definieren
  message(F("Achsen referenziert."), F("Achsen referenziert."), MESSAGE_TYPE_INFO);

  if (!SD.begin(SD_SELECT))
    message(F("Initialisierung der SD-Karte fehlgeschlagen!"), F("Initialisierung der SD-Karte SD-Karte fehlgeschlagen! Keine automatische Messung mit Speicherung auf SD-Karte möglich!"), MESSAGE_TYPE_CONFIRMATION);
  else
    Sd_inited = true;

  Serial.println(F("Fertig!"));

  // Laden von Einstellungen der automatischen Messung
  EEPROM.get(eeprom_AM_Inkrement_D_G, AM_Inkrement_D_G);
  EEPROM.get(eeprom_AM_Inkrement_D_K, AM_Inkrement_D_K);
  EEPROM.get(eeprom_AM_Inkrement_X_K, AM_Inkrement_X_K);
  EEPROM.get(eeprom_AM_Inkrement_X_G, AM_Inkrement_X_G);
  EEPROM.get(eeprom_AM_filename, AM_filename);
  EEPROM.get(eeprom_AM_Amount_Smallloops_D, AM_Amount_Smallloops_D);
  EEPROM.get(eeprom_AM_Amount_Smallloops_X, AM_Amount_Smallloops_X);
  EEPROM.get(eeprom_AM_mirrormode, AM_mirrormode);
  EEPROM.get(eeprom_AM_Strut_first, AM_Strut_first);
  EEPROM.get(eeprom_AM_Strut_last, AM_Strut_last);
  EEPROM.get(eeprom_AM_D_Bot_S, AM_D_Bot_S);
  EEPROM.get(eeprom_AM_D_Start_S, AM_D_Start_S);
  EEPROM.get(eeprom_AM_X_Start_S, AM_X_Start_S);
  EEPROM.get(eeprom_AM_X_Max_S, AM_X_Max_S);
  EEPROM.get(eeprom_AM_decimalpoint, AM_decimalpoint);
  EEPROM.get(eeprom_AM_MachineZ, AM_MachineZ);

  // Referenz laden
  EEPROM.get(eeprom_Ref_PosMMM, Ref_PosMMM);
  EEPROM.get(eeprom_Ref_PosDStep, Ref_PosDStep);
  EEPROM.get(eeprom_Ref_PosXStep, Ref_PosXStep);
  EEPROM.get(eeprom_Ref_Height, Ref_Height);

  // Initialisiere Menü
  _move_function = move_function;

  // Verfahren
  main_menu.elements[0]->fun = [](bool serial) {
    direct_move_control();
    return true;
  };

  // Dateibrowser
  main_menu.elements[1]->fun = [](bool serial) {
    filebrowser();
    return true;
  };

  // Einstellungen setzen
  main_menu.elements[settings_index]->menu->elements[0]->fun = [](bool serial) {
    Set_MObject_Reference();
    return true;
  };
  main_menu.elements[settings_index]->menu->elements[1]->variable = new Variable(Variable::Type::UINT16, F("Untere D-Grenze"), &d_low, eeprom_d_low);

  // Einstellungen - Stepper
  main_menu.elements[settings_index]->menu->elements[settings_steppers_index]->menu->elements[0]->variable = new Variable(Variable::Type::UINT16, F("Geschwindigkeit X"), &stepper_maxspeed_x, eeprom_stepper_maxspeed_x);
  main_menu.elements[settings_index]->menu->elements[settings_steppers_index]->menu->elements[1]->variable = new Variable(Variable::Type::UINT16, F("Geschwindigkeit D"), &stepper_maxspeed_d, eeprom_stepper_maxspeed_d);
  main_menu.elements[settings_index]->menu->elements[settings_steppers_index]->menu->elements[2]->variable = new Variable(Variable::Type::UINT16, F("Beschleunigung X"), &stepper_acceleration_x, eeprom_stepper_acceleration_x);
  main_menu.elements[settings_index]->menu->elements[settings_steppers_index]->menu->elements[3]->variable = new Variable(Variable::Type::UINT16, F("Beschleunigung D"), &stepper_acceleration_d, eeprom_stepper_acceleration_d);
  main_menu.elements[settings_index]->menu->elements[settings_steppers_index]->menu->elements[4]->variable = new Variable(Variable::Type::FLOAT, F("Skalierung X"), &ScalingX, eeprom_scalingx);
  main_menu.elements[settings_index]->menu->elements[settings_steppers_index]->menu->elements[5]->variable = new Variable(Variable::Type::FLOAT, F("Skalierung D"), &ScalingD, eeprom_scalingd);
  main_menu.elements[settings_index]->menu->elements[settings_steppers_index]->menu->elements[6]->fun = [](bool serial) { reboot(); return true;};

  main_menu.elements[settings_index]->menu->elements[3]->variable = new Variable(Variable::Type::UINT16, F("Blockhöhe in mm"), &block_height, eeprom_block_height);
  main_menu.elements[settings_index]->menu->elements[4]->variable = new Variable(Variable::Type::UINT16, F("Mindestmesswert vom Laser in mm"), &am_minimal_measurement, eeprom_am_minimal_measurement);

  // Aktuelle Daten
  main_menu.elements[aktuelledaten_index]->menu->elements[0]->fun = [](bool serial) {
    Serial.println(F(""));
    Serial.println(F("Aktuelle Daten:"));
    Serial.print(F("GETPOSDSTEP: ")); Serial.print(GETPOSDSTEP()); Serial.print(F(" steps\nGETPOSXSTEP: ")); Serial.print(GETPOSXSTEP()); Serial.print(F(" steps\nRef_PosXStep: ")); Serial.print(Ref_PosXStep);
    Serial.print(F(" steps\nRef_PosMMM: ")); Serial.print(Ref_PosMMM); Serial.print(F(" mm\nRef_PosDStep: ")); Serial.print(Ref_PosDStep); Serial.print(F(" steps\nRef_Height: ")); Serial.print(Ref_Height);
    Serial.print(F(" mm\nfirstStrut: ")); Serial.print(AM_Strut_first); Serial.print(F(" steps\nlastStrut: ")); Serial.print(AM_Strut_last); Serial.print(F(" steps\nInkrement_D_K: ")); Serial.print(AM_Inkrement_D_K);
    Serial.print(F(" mm\nInkrement_D_G: ")); Serial.print(AM_Inkrement_D_G); Serial.print(F(" mm\nInkrement_X_G: ")); Serial.print(AM_Inkrement_X_G); Serial.print(F(" mm\nInkrement_X_K: ")); Serial.print(AM_Inkrement_X_K); Serial.print(F(" mm\nAM_MachineZ: ")); Serial.print(AM_MachineZ);
    Serial.print(F(" mm\nblock_height: ")); Serial.print(block_height); Serial.print(F(" mm\nAM_mirrormode: ")); Serial.print(AM_mirrormode); Serial.print(F("\nAM_X_Max_S: ")); Serial.println(AM_X_Max_S);
    return true;
  };

  main_menu.elements[aktuelledaten_index]->menu->elements[1]->fun = [](bool serial) {
    uint16_t limit = EEPROM_SIZE - 1;
    while (EEPROM.read(limit) == 255)
      limit--;
    Serial.print(limit);
    Serial.print(F("\nEEPROM:\n"));
    Serial.print(F("   x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 xA xB xC xD xE xF\n"));
    for (uint16_t i = 0; i < (limit + 15) / 16; i++) {
      Serial.print(i); Serial.print(F("x "));
      for (uint16_t j = 0; j < 16; j++) {
        if (EEPROM.read(i * 16 + j) < 16)
          Serial.print(F("0"));
        Serial.print(EEPROM.read(i * 16 + j), HEX);
        Serial.print(F(" "));
      }
      Serial.print(F("\n"));
    }
    return true;
  };
  main_menu.elements[aktuelledaten_index]->menu->elements[2]->fun = [](bool serial) {
    Serial.println(F("Referenzvariablen:"));
    Serial.print(F("Ref_PosXStep = ")); Serial.println(Ref_PosXStep);
    Serial.print(F("Ref_PosMMM = ")); Serial.println(Ref_PosMMM);
    Serial.print(F("Ref_PosDStep = ")); Serial.println(Ref_PosDStep);
    Serial.print(F("Ref_Height = ")); Serial.println(Ref_Height);
    return true;
  };
  main_menu.elements[aktuelledaten_index]->menu->elements[3]->fun = [](bool serial) {
    Serial.println(F("Stepperwerte:"));
    Serial.print(F("stepper_maxspeed_x = ")); Serial.println(stepper_maxspeed_x);
    Serial.print(F("stepper_maxspeed_d = ")); Serial.println(stepper_maxspeed_d);
    Serial.print(F("stepper_acceleration_x = ")); Serial.println(stepper_acceleration_x);
    Serial.print(F("stepper_acceleration_d = ")); Serial.println(stepper_acceleration_d);
    return true;
  };

  // Automessung
  // Automessung - Inkrement
  main_menu.elements[automaticmeasure_index]->menu->elements[inkrement_index]->menu->elements[0]->variable = new Variable(Variable::Type::UINT16, F("Inkrement X gross"), &AM_Inkrement_X_G, eeprom_AM_Inkrement_X_G);
  main_menu.elements[automaticmeasure_index]->menu->elements[inkrement_index]->menu->elements[1]->variable = new Variable(Variable::Type::UINT16, F("Inkrement X klein"), &AM_Inkrement_X_K, eeprom_AM_Inkrement_X_K);
  main_menu.elements[automaticmeasure_index]->menu->elements[inkrement_index]->menu->elements[2]->variable = new Variable(Variable::Type::UINT16, F("Anzahl feiner Schritte X"), &AM_Amount_Smallloops_X, eeprom_AM_Amount_Smallloops_X);
  main_menu.elements[automaticmeasure_index]->menu->elements[inkrement_index]->menu->elements[3]->variable = new Variable(Variable::Type::UINT16, F("Inkrement D gross"), &AM_Inkrement_D_G, eeprom_AM_Inkrement_D_G);
  main_menu.elements[automaticmeasure_index]->menu->elements[inkrement_index]->menu->elements[4]->variable = new Variable(Variable::Type::UINT16, F("Inkrement D klein"), &AM_Inkrement_D_K, eeprom_AM_Inkrement_D_K);
  main_menu.elements[automaticmeasure_index]->menu->elements[inkrement_index]->menu->elements[5]->variable = new Variable(Variable::Type::UINT16, F("Anzahl feiner Schritte D"), &AM_Amount_Smallloops_D, eeprom_AM_Amount_Smallloops_D);

  // Automessung - Streben
  main_menu.elements[automaticmeasure_index]->menu->elements[streben_index]->menu->elements[0]->variable = new Variable(Variable::Type::POSITION, F("Erste Strebe setzen"), new Variable::T_Position(Stepper_Part::X, &AM_Strut_first, eeprom_AM_Strut_first));
  main_menu.elements[automaticmeasure_index]->menu->elements[streben_index]->menu->elements[1]->variable = new Variable(Variable::Type::POSITION, F("Letzte Strebe setzen"), new Variable::T_Position(Stepper_Part::X, &AM_Strut_last, eeprom_AM_Strut_last));
  main_menu.elements[automaticmeasure_index]->menu->elements[streben_index]->menu->elements[2]->fun = [](bool serial) {
    set_struts_height();
    return true;
  };

  // Automessung - Dateinamen setzen
  main_menu.elements[automaticmeasure_index]->menu->elements[2]->fun = [](bool serial) {
    set_filename();
    return true;
  };
  main_menu.elements[automaticmeasure_index]->menu->elements[3]->variable = new Variable(Variable::Type::SELECTION, F("Dezimaltrennzeichen:"), new Variable::T_Select((uint8_t*)(&AM_decimalpoint), 2, automaticmeasure_decimalpoint_options), eeprom_AM_decimalpoint);

  // Automessung - Grenzen setzen
  main_menu.elements[automaticmeasure_index]->menu->elements[border_index]->menu->elements[0]->fun = set_start_position;
  main_menu.elements[automaticmeasure_index]->menu->elements[border_index]->menu->elements[1]->variable = new Variable(Variable::Type::POSITION, F("Grenze X Ende"), new Variable::T_Position(Stepper_Part::X, &AM_X_Max_S, eeprom_AM_X_Max_S));
  main_menu.elements[automaticmeasure_index]->menu->elements[border_index]->menu->elements[2]->variable = new Variable(Variable::Type::POSITION, F("Grenze D Unten"), new Variable::T_Position(Stepper_Part::D, nullptr, 0, &AM_D_Bot_S, eeprom_AM_D_Bot_S));

  // Automessung - Spiegelmodus
  main_menu.elements[automaticmeasure_index]->menu->elements[5]->variable = new Variable(Variable::Type::SELECTION, F("Spiegelmodus"), new Variable::T_Select((uint8_t*)(&AM_mirrormode), 3, automaticmeasure_mirrormode_options), eeprom_AM_mirrormode);

  // Automessung - Start Automessung
  main_menu.elements[automaticmeasure_index]->menu->elements[6]->fun = [](bool serial) {
    automatic_measure_main();
    return true;
  };

  // Tests
  main_menu.elements[tests_index]->menu->elements[0]->fun = [](bool serial) {
    X_Axis_test();
    return true;
  };
  main_menu.elements[tests_index]->menu->elements[1]->fun = [](bool serial) {
    D_Axis_test();
    return true;
  };
  main_menu.elements[tests_index]->menu->elements[2]->fun = [](bool serial) {
    Length_test();
    return true;
  };
  main_menu.elements[tests_index]->menu->elements[3]->fun = [](bool serial) {
    Precision_test();
    return true;
  };

  // Neustart
  main_menu.elements[6]->fun = [](bool serial) {
    reboot();
    return true;
  };
}

void loop() {
  Menu* cur_menu = &main_menu;
  uint8_t menu_position = 0;
  bool newSerial = true; // True if input by serial monitor -> serial text is printed

  while (1) {
    if (newSerial) {
      Serial.print(F("\nx - senden, um Menüeintrag x auszuwählen\n"));
      Menu::Element* par = cur_menu->parent;
      char menu_name[100] = "Hauptmenu";
      while (par != NULL) {
        sprintf(menu_name, "%s - %s", menu_name, par->title);
        par = par->parent->parent;
      }
      Serial.print(menu_name);
      Serial.print(F(":\n"));

      for (uint8_t i = 0; i < cur_menu->am_elements; i++) {
        Serial.print(i + 1);
        Serial.print(F(" - "));
        Serial.println(cur_menu->elements[i]->title);
      }
      newSerial = false;
    }
    lcd_print(cur_menu->elements[menu_position]->title, cur_menu->elements[menu_position + 1]->title);
    lcd.setCursor(15, 0);
    lcd.print(F("<"));

    uint8_t TastenStatus;
    while ((TastenStatus = Tasterstatus()) == SwitchNone); // Warten auf Tasterinput

    if (TastenStatus & SwitchSourceSerial) {
      if (last_read_char == 'q')
        newSerial = cur_menu->elements[last_read_char - '1']->leave(&cur_menu, true);
      else if (last_read_char > '0' && last_read_char < cur_menu->am_elements + '1')
        newSerial = cur_menu->elements[last_read_char - '1']->enter(&cur_menu, true);
      else if (last_read_char != '\n') {
        ;//Serial.print(F("Falsche Eingabe (")); Serial.print((uint8_t)last_read_char); Serial.println(F(")!")); // Rausgelöscht um Spam durch den ESP zu vermeiden
      }
    }

    switch (TastenStatus & SwitchFilter) {
    case SwitchTop:
      menu_position = menu_position ? menu_position - 1 : MENU_POSITIONS - 1;
      break;
    case SwitchRight:
      cur_menu->elements[menu_position]->enter(&cur_menu, false);
      break;
    case SwitchBottom:
      menu_position = menu_position < MENU_POSITIONS - 1 ? menu_position + 1 : 0;
      break;
    case SwitchLeft:
      cur_menu->elements[menu_position]->leave(&cur_menu, false);
      break;
    }
  }
}

/**
 *
 **/
void automatic_measure_main(bool serial) {
  if (!Sd_inited) {
    if (MESSAGE_RESPONSE_YES != message(F("SD Karte nicht initialisiert! Trotzdem starten?"), F("SD-Karte nicht initialisiert. Messung trotzdem starten? Messdaten werden nicht gespeichert!"), MESSAGE_TYPE_YESNOBACK))
      return;
  }

  char** buffer = (char**)malloc(sizeof(char*));
  uint_fast32_t starttime = millis();
  uint_fast16_t am_points = 0;
  char* filename_value = (char*)malloc(13 * sizeof(char));
  char* filename_log = (char*)malloc(13 * sizeof(char));

  if (automatic_measure(serial, buffer, am_points, filename_value, filename_log)) {
    Serial.println(F("!>")); // End silent mode on Telegram
    File sd = SD.open(filename_value, FILE_WRITE);
    sd.println(F("EndLoft"));
    sd.println(F("End"));
    sd.close();
    Serial.print(F("Starttime: ")); Serial.println(starttime);
    starttime = (millis() - starttime) / 1000;
    Serial.print(F("Endtime: ")); Serial.println(millis());
    Serial.print(F("Deltatime: ")); Serial.println(starttime);
    Serial.print(F("Punkte aufgenommen: ")); Serial.println(am_points);
    char time[9];
    sprintf(time, "%02lu:%02lu:%02lu", starttime / 60 / 60, (starttime % 3600) / 60, starttime % 60);
    Serial.print(F("Benötigte Zeit: ")); Serial.println(time);
    Serial.print(F("Punkte / Minute: ")); Serial.print(am_points / (starttime / 60)); Serial.println(F(" n/min"));
    message(F("Messung abgeschlossen"), F("Messung abgeschlossen!"), MESSAGE_TYPE_CONFIRMATION);
  } else {
    Serial.print(F("!>")); // End silent mode on Telegram

  }
  free(*buffer);
  free(buffer);
  free(filename_value);
  free(filename_log);
}

/*  automatic_measure - Führt einen vollständigen Messungsdurchlauf durch
    Parameter:
     serial (=false) - True, wenn serieller Eingabemodus (Bestätigungs-Nachrichten werden auf LCD nicht angezeigt)
    Rückgabewert:
    -
*/
bool automatic_measure(bool serial, char** buffer_return, uint_fast16_t& am_points, char* filename_value, char* filename_log) {
  message(F("Automatische Messung"), F("\n----------------------------------------------------\nAutomatische Messung\n"), MESSAGE_TYPE_CONFIRMATION);

  const uint_fast8_t am_big_steps = (AM_Inkrement_D_K * AM_Amount_Smallloops_D + AM_Inkrement_D_G - 1) / AM_Inkrement_D_G;

  enum Mode : uint_fast8_t { Suche_rev, Suche, Anfangsklein, Haupt, Endklein } mode = Mode::Anfangsklein;

  int_fast32_t x = AM_X_Start_S;
  uint_fast16_t d = AM_D_Start_S; uint_fast16_t last_d = d;

  Serial.println(F("Gewählte Parameter:             (steps, C-Variablenname)"));
  Serial.print(F("Startpunkt:\n"));
  Serial.print(F("Längs x:              ")); Serial.print(SCALEXSTM(AM_X_Start_S)); Serial.print(F(" mm (= ")); Serial.print(AM_X_Start_S); Serial.print(F(" steps, AM_X_Start_S)\n"));
  Serial.print(F("Quer d:               ")); Serial.print(SCALEDSTM(AM_D_Start_S)); Serial.print(F(" mm (= ")); Serial.print(AM_D_Start_S); Serial.print(F(" steps, AM_D_Start_S)\n"));
  Serial.print(F("\nGrenzen:\n"));
  Serial.print(F("Unten (D):            ")); Serial.print(SCALEDSTM(AM_D_Bot_S)); Serial.print(F(" mm (=")); Serial.print(AM_D_Bot_S); Serial.print(F(" steps, AM_D_Bot_S)\n"));
  Serial.print(F("Längs (X):            ")); Serial.print(SCALEXSTM(AM_X_Max_S)); Serial.print(F(" mm (=")); Serial.print(AM_X_Max_S); Serial.print(F(" steps, AM_X_Max_S)\n"));
  Serial.print(F("\nStreben:\n"));
  Serial.print(F("Erste:                ")); Serial.print(SCALEXSTM(AM_Strut_first)); Serial.print(F(" mm (=")); Serial.print(AM_Strut_first); Serial.print(F(" steps, AM_Strut_first)\n"));
  Serial.print(F("Letzte:               ")); Serial.print(SCALEXSTM(AM_Strut_last)); Serial.print(F(" mm (=")); Serial.print(AM_Strut_last); Serial.print(F(" steps, AM_Strut_last)\n"));
  Serial.print(F("\nInkremente:\n"));
  Serial.print(F("X groß:               ")); Serial.print(AM_Inkrement_X_G); Serial.print(F(" mm (=")); Serial.print(SCALEXMTS(AM_Inkrement_X_G)); Serial.print(F(" steps, AM_Inkrement_X_G)\n"));
  Serial.print(F("X klein:              ")); Serial.print(AM_Inkrement_X_K); Serial.print(F(" mm (=")); Serial.print(SCALEXMTS(AM_Inkrement_X_K)); Serial.print(F(" steps, AM_Inkrement_X_K)\n"));
  Serial.print(F("D groß:               ")); Serial.print(AM_Inkrement_D_G); Serial.print(F(" mm (=")); Serial.print(SCALEDMTS(AM_Inkrement_D_G)); Serial.print(F(" steps, AM_Inkrement_D_G)\n"));
  Serial.print(F("D klein:              ")); Serial.print(AM_Inkrement_D_K); Serial.print(F(" mm (=")); Serial.print(SCALEDMTS(AM_Inkrement_D_K)); Serial.print(F(" steps, AM_Inkrement_D_K)\n"));
  Serial.print(F("\nWeiteres:\n"));
  Serial.print(F("Größe Kleinschleife D:")); Serial.print(AM_Amount_Smallloops_D); Serial.print(F(" (AM_Amount_Smallloops_D)\n"));
  Serial.print(F("Größe Kleinschleife X:")); Serial.print(AM_Amount_Smallloops_X); Serial.print(F(" (AM_Amount_Smallloops_X)\n"));
  Serial.print(F("Größe Pufferspeicher: ")); Serial.print(am_big_steps); Serial.print(F(" (am_big_steps)\n"));
  Serial.print(F("Spiegelmodus:         ")); Serial.println(AM_mirrormode == Mirrormode::Normal ? F("Normal\n") : (AM_mirrormode == Mirrormode::Gantry_rotated ? F("Gantrydrehung\n") : F("D-Achsen-Spiegelung\n")));
  Serial.print(F("Minimaler Messwert:   ")); Serial.print(am_minimal_measurement); Serial.print(F(" (am_minimal_measurement)\n"));

  bool error = false;
  if (AM_Strut_first == 0 || AM_Strut_last == 0) {
    Serial.println(F("Streben nicht gesetzt!"));
    error = true;
  }
  if (AM_X_Max_S == 0 || AM_D_Bot_S == 0) {
    Serial.println(F("Grenzen nicht gesetzt!"));
    error = true;
  }
  if (AM_X_Start_S == 0 || AM_D_Start_S == 0) {
    Serial.println(F("Startpunkt nicht gesetzt!"));
    error = true;
  }
  if (AM_Inkrement_D_G == 0 || AM_Inkrement_D_K == 0 || AM_Inkrement_X_G == 0 || AM_Inkrement_X_K == 0) {
    Serial.println(F("Inkrement nicht gesetzt!"));
    error = true;
  }

  if (error) {
    if (message(F("Nicht konfiguriert!"), F("Noch nicht voll konfiguriert! Fehlende Konfigurationen über das Menü setzen und Messung erneut starten. Alternativ: Messung mit aktuellen Parametern trotzdem starten (Vorsicht!)?"), MESSAGE_TYPE_YESNOBACK) != MESSAGE_RESPONSE_YES)
      return false;
  }

  uint_fast8_t filename_length = strlen(AM_filename);
  File sd;
  strcpy(filename_value, AM_filename);
  strcat(filename_value, "A.txt");

  while (SD.exists(filename_value) && filename_value[filename_length] < 'Z') {
    filename_value[filename_length]++;
  }

  if (filename_value[filename_length] == 'Z') {
    message(F("Dateinamen vergeben, Dateien loeschen!"), F("Alle Dateinamen bereits vergeben, Alte Dateien löschen!"), MESSAGE_TYPE_CONFIRMATION);
    return false;
  }

  if (filename_value[filename_length] != 'A') {
    uint_fast8_t response = message(F("Ausgabedatei bereits vorhanden, Messung fortfuehren?"), F("Ausgabedatei ist bereits vorhanden! Soll die letzte Messung fortgeführt werden?"), MESSAGE_TYPE_YESNOBACK);
    if (response == MESSAGE_RESPONSE_BACK)
      return false;
    else if (response == MESSAGE_RESPONSE_YES) { // Letzte Messung fortführen
      filename_value[filename_length]--;
      // X-Wert bestimmen
      sd = SD.open(filename_value, FILE_READ);
      uint32_t s = sd.size();
      sd.seek(s--);
      char c = sd.peek();
      while (c < '0' || c > '9') { // Die letzte Zeile mit Zahlen suchen
        sd.seek(--s);
        c = sd.peek();
      }
      while (c != '\n') { // Die letzte Zeile mit Zahlen bis zum Anfangszeilenumbruch zurückgehen
        sd.seek(--s);
        c = sd.peek();
      }
      sd.seek(++s);
      c = sd.peek();
      if (c == '-') { // Ggf. vorhandenes Minus-Zeichen entfernen
        sd.seek(++s);
        c = sd.peek();
      }
      x = 0;
      while (c >= '0' && c <= '9') { // Zahl auswerten
        x *= 10;
        x += c - '0';
        sd.seek(++s);
        c = sd.peek();
      }
      sd.close();
      x = SCALEXMTS(x) + Ref_PosXStep;
      d = 0;
      mode = Mode::Suche;
      Serial.print(F("Führe letzte Messung fort\n"));
    } else { // Neue Messung starten
      Serial.print(F("Starte neue Messung\n"));
    }
  }

  strcpy(filename_log, filename_value);
  filename_log[filename_length] += 'a' - 'A';

  Serial.print(F("Werte-Datei: '")); Serial.print(filename_value); Serial.print(F("', Log-Datei: '")); Serial.print(filename_log); Serial.print(F("'\n"));

  sd = SD.open(filename_log, FILE_READ);
  sd.println(F("Gewählte Parameter:             (steps, C-Variablenname)"));
  sd.print(F("Startpunkt:\n"));
  sd.print(F("Längs x:              ")); sd.print(SCALEXSTM(AM_X_Start_S)); sd.print(F(" mm (= ")); sd.print(AM_X_Start_S); sd.print(F(" steps, AM_X_Start_S)\n"));
  sd.print(F("Quer d:               ")); sd.print(SCALEDSTM(AM_D_Start_S)); sd.print(F(" mm (= ")); sd.print(AM_D_Start_S); sd.print(F(" steps, AM_D_Start_S)\n"));
  sd.print(F("\nGrenzen:\n"));
  sd.print(F("Unten (D):            ")); sd.print(SCALEDSTM(AM_D_Bot_S)); sd.print(F(" mm (=")); sd.print(AM_D_Bot_S); sd.print(F(" steps, AM_D_Bot_S)\n"));
  sd.print(F("Längs (X):            ")); sd.print(SCALEXSTM(AM_X_Max_S)); sd.print(F(" mm (=")); sd.print(AM_X_Max_S); sd.print(F(" steps, AM_X_Max_S)\n"));
  sd.print(F("\nStreben:\n"));
  sd.print(F("Erste:                ")); sd.print(SCALEXSTM(AM_Strut_first)); sd.print(F(" mm (=")); sd.print(AM_Strut_first); sd.print(F(" steps, AM_Strut_first)\n"));
  sd.print(F("Letzte:               ")); sd.print(SCALEXSTM(AM_Strut_last)); sd.print(F(" mm (=")); sd.print(AM_Strut_last); sd.print(F(" steps, AM_Strut_last)\n"));
  sd.print(F("\nInkremente:\n"));
  sd.print(F("X groß:               ")); sd.print(AM_Inkrement_X_G); sd.print(F(" mm (=")); sd.print(SCALEXMTS(AM_Inkrement_X_G)); sd.print(F(" steps, AM_Inkrement_X_G)\n"));
  sd.print(F("X klein:              ")); sd.print(AM_Inkrement_X_K); sd.print(F(" mm (=")); sd.print(SCALEXMTS(AM_Inkrement_X_K)); sd.print(F(" steps, AM_Inkrement_X_K)\n"));
  sd.print(F("D groß:               ")); sd.print(AM_Inkrement_D_G); sd.print(F(" mm (=")); sd.print(SCALEDMTS(AM_Inkrement_D_G)); sd.print(F(" steps, AM_Inkrement_D_G)\n"));
  sd.print(F("D klein:              ")); sd.print(AM_Inkrement_D_K); sd.print(F(" mm (=")); sd.print(SCALEDMTS(AM_Inkrement_D_K)); sd.print(F(" steps, AM_Inkrement_D_K)\n"));
  sd.print(F("\nWeiteres:\n"));
  sd.print(F("Größe Kleinschleife X:")); sd.print(AM_Amount_Smallloops_X); sd.print(F(" (AM_Amount_Smallloops_X)\n"));
  sd.print(F("Größe Kleinschleife D:")); sd.print(AM_Amount_Smallloops_D); sd.print(F(" (AM_Amount_Smallloops_D)\n"));
  sd.print(F("Größe Pufferspeicher: ")); sd.print(am_big_steps); sd.print(F(" (am_big_steps)\n"));
  sd.print(F("Spiegelmodus:         ")); sd.println(AM_mirrormode == Mirrormode::Normal ? F("Normal\n") : (AM_mirrormode == Mirrormode::Gantry_rotated ? F("Gantrydrehung\n") : F("D-Achsen-Spiegelung\n")));
  sd.print(F("Minimaler Messwert:   ")); sd.print(am_minimal_measurement); sd.print(F(" (am_minimal_measurement)\n"));
  sd.close();

  Serial.print(F("Starte bei x = ")); Serial.print(SCALEXSTM(x));  Serial.print(F(" mm (= ")); Serial.print(x); Serial.print(F(" steps)\n"));

  uint_fast16_t Inkrement_D_G_S = SCALEDMTS(AM_Inkrement_D_G);
  uint_fast16_t Inkrement_D_K_S = SCALEDMTS(AM_Inkrement_D_K);
  int_fast16_t Inkrement_X_G_S = (AM_mirrormode == Mirrormode::Gantry_rotated ? -1 : 1) * (int_fast16_t)SCALEXMTS(AM_Inkrement_X_G);
  int_fast16_t Inkrement_X_K_S = (AM_mirrormode == Mirrormode::Gantry_rotated ? -1 : 1) * (int_fast16_t)SCALEXMTS(AM_Inkrement_X_K);
  last_d = d;

  if (AM_decimalpoint == Decimalpoint::Comma) {
    AM_value_decimalpoint = ',';
    AM_value_separator = '.';
  } else {
    AM_value_decimalpoint = '.';
    AM_value_separator = ',';
  }

  uint_fast16_t line_width = (SCALEDSTM(AM_D_Bot_S) - 2 * AM_Amount_Smallloops_D * AM_Inkrement_D_K) / AM_Inkrement_D_G + 2 * AM_Amount_Smallloops_D;
  line_width *= POINT_SIZE; // Anzahl Zeichen pro Koordinate ("XXXXX,XXX.XX,XXX.XX\n")
  line_width += 11 + 9; // "StartCurve\n" und "EndCurve\n"
  char* line_buffer = (char*)malloc(line_width * sizeof(char));
  *buffer_return = line_buffer;
  if (line_buffer == NULL) {
    Serial.print(line_width); Serial.print(F(" Byte benötigt. "));
    message(F("Abbruch: Zu feine Aufloesung!"), F("Auflösung zu fein gewählt: Nicht genug Speicher für Linienpuffer vorhanden! Auflösung verringern. Messung abgebrochen."), MESSAGE_TYPE_CONFIRMATION);
    return false;
  }
  char* line_buffer_pos = line_buffer;

  uint_fast8_t loop_counter = 0;
  int_fast32_t small_x_end = AM_X_Start_S + Inkrement_X_K_S * AM_Amount_Smallloops_X;
  int_fast32_t small_x_start = AM_X_Max_S - Inkrement_X_K_S * AM_Amount_Smallloops_X;

  uint_fast16_t last_measure;

  if (message(F("Messung starten?"), F("Messung jetzt starten?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_NO)
    return false;

  message(F("Referenziere System ..."), F("Referenziere System ..."), MESSAGE_TYPE_INFO);
  referenceAxis();

  if (check_state(0)) // Stopptaster
    return false;

  sd = SD.open(filename_value, FILE_WRITE);
  if (mode == Mode::Anfangsklein) { // Neue Messung
    sd.println(F("StartLoft"));
  } else { // Messung fortgeführt

  }
  sd.close();


  message(F("Automatische Messung gestartet"), F("Automatische Messung gestartet! Unterbrechen mittels Stop-Taster!\n"), MESSAGE_TYPE_INFO);
  stepperMoveTo(x, d);

  Serial.println(F("!<")); // Start silent mode on Telegram

  while (1) { // Haupt-Schleife
    if (check_state(x)) // Stoptaster
      return false;

    while (!manual_measure_int(&last_measure) && last_measure > am_minimal_measurement) { // Messung fehlgeschlagen
      stepperMoveTo(x, ++d);
      message(F("Messung w. fehlgeschlagen!"), F("!Messung wiederholt fehlgeschlagen!"), MESSAGE_TYPE_INFO);
      sd = SD.open(filename_log, FILE_WRITE);
      sd.print(F("E0:")); sd.print(x); sd.print(F(";")); sd.print(d); sd.print(F(" (")); sd.print(last_measure); sd.println(F(")"));
      if (d > AM_D_Bot_S) {
        sd.println(F("Keine korrekten Messwerte, Messung abgebrochen!"));
        sd.close();
        Serial.println(F("!Keine korrekten Messwerte, Messung abgebrochen!"));
        return false;
      }
      sd.close();
    }

    if (check_state(x)) // Stoptaster
      return false;

    switch (mode) {
    case Mode::Suche_rev: // Rückwärtssuche, bis Objekt verschwunden
      Serial.print(F("mode_suche_rev    - "));
      if (ZOK(x, d, last_measure)) {
        d -= Inkrement_D_G_S;
        break;
      } else {
        mode = Mode::Suche;
        //message(F("Suche Messobjekt ..."), F("Suche Messobjekt feinschrittig ..."), MESSAGE_TYPE_INFO);
      }

    case Mode::Suche:
      Serial.print(F("mode_suche        - "));
      if (!ZOK(x, d, last_measure)) {
        d += Inkrement_D_K_S;
        if (d > AM_D_Bot_S) { // Kein Boot gefunden -> Messungsende
          return true;
        }
        break;
      } else {
        mode = Mode::Anfangsklein;
        loop_counter = 1;
        last_d = d;
        //message(F("Suche Anfangskleinschleife ..."), F("Ende Rückwärtssuche -> Beginne Anfangskleinschleife ..."), MESSAGE_TYPE_INFO);
      }

    case Mode::Anfangsklein:
      Serial.print(F("mode_anfangsklein - "));
      if (ZOK(x, d, last_measure)) { // Wert okay
        if (loop_counter < AM_Amount_Smallloops_D) {
          line_buffer_pos = saveCoord(line_buffer_pos, GETPOSRMM(), GETPOSSMMF(last_measure), GETPOSTMMF(last_measure), &am_points, line_width, line_buffer);
          d += Inkrement_D_K_S;
          loop_counter++;
          break;
        } else { // AKS vollständig
          loop_counter = 0;
          mode = Mode::Haupt;
          //message(F("Anfangskleinschleife Ende ..."), F("Ende AKS -> Beginne Hauptschleife ..."), MESSAGE_TYPE_INFO);
        }
      } else { // Messobjekt zu Ende
        mode = Mode::Endklein;
        Serial.print(F("Messobjekt in AKS zu Ende\n"));
      }

    case Mode::Haupt:
      Serial.print(F("mode_haupt        - "));
      if (ZOK(x, d, last_measure)) { // Wert okay
        line_buffer_pos = saveCoord(line_buffer_pos, GETPOSRMM(), GETPOSSMMF(last_measure), GETPOSTMMF(last_measure), &am_points, line_width, line_buffer);
        loop_counter++;
        /*        if (d + am_big_steps * Inkrement_D_G_S > AM_D_Bot_S) { // Fest definiertes Ende ist nah, direkte Endkleinmessung kann gestartet werden
                  mode = Mode::Endklein; Das funktioniert so aktuell nicht, denn wenn das Ende des Bootes "unerwartet" zu früh in der EKS kommt, dann springt er nicht mehr den großen Schritt zurück und erhält entsprechend zu wenige EKS Punkte
                  d += Inkrement_D_K_S;
                } else {*/
        d += Inkrement_D_G_S;
        //}
        break;
      } else {
        if (mode != Mode::Endklein) {
          uint16_t am_steps_back = 0;

          // Prüfen, wie viele große Schritte bereits gefahren worden
          if ((loop_counter - 1) * Inkrement_D_G_S <= (AM_Amount_Smallloops_D - 1) * Inkrement_D_K_S) { // Großschrittbereich ist kleiner als der gewünschte Kleinschrittbereich, fahre kompletten Großschrittbereich zurück
            d = d - loop_counter * Inkrement_D_G_S + Inkrement_D_K_S;
            am_steps_back = loop_counter;
          } else { // Fahre zum letzten getroffenen großen Schritt zurück und dann n-1 kleine Schritte weiter, sodass mindestens n kleine Schritte getroffen werden
            d = d - Inkrement_D_G_S - (AM_Amount_Smallloops_D - 1) * Inkrement_D_K_S;
            am_steps_back = am_big_steps;
          }

          line_buffer_pos--;

          // Die letzten großen Schritte löschen zum feinschrittig aufnehmen
          while (am_steps_back && line_buffer_pos > line_buffer) {
            line_buffer_pos--;

            if (*line_buffer_pos == '\n')
              am_steps_back--;
          }

          line_buffer_pos++;

          mode = Mode::Endklein;
          //message(F("Hauptschleife Ende ..."), F("Ende Hauptschleife -> Beginne Endkleinschleife ..."), MESSAGE_TYPE_INFO);
          break;
        }
      }

    case Mode::Endklein:
      Serial.print(F("mode_endklein     - "));
      if (ZOK(x, d, last_measure)) { // Wert okay
        line_buffer_pos = saveCoord(line_buffer_pos, GETPOSRMM(), GETPOSSMMF(last_measure), GETPOSTMMF(last_measure), &am_points, line_width, line_buffer);
        d += Inkrement_D_K_S;
      } else {
        message(F("Spalte abgeschlossen!"), F("!Spalte abgeschlossen, speichere Daten ..."), MESSAGE_TYPE_INFO);
        sd = SD.open(filename_value, FILE_WRITE);
        sd.println(F("StartCurve"));
        sd.print(line_buffer);
        sd.println(F("EndCurve"));
        sd.close();
        Serial.println(line_buffer);
        line_buffer_pos = line_buffer;

        if (x < small_x_end || x > small_x_start) {
          x += Inkrement_X_K_S;
        } else {
          x += Inkrement_X_G_S;
        }
        d = last_d - 2 * Inkrement_D_K_S;

        if (AM_mirrormode == Mirrormode::Gantry_rotated ? ((uint_fast32_t)x < AM_X_Max_S) : ((uint_fast32_t)x > AM_X_Max_S)) { // X-Achsen-Grenze erreicht, Beende Messung
          return true;
        }

        stepperMoveTo(0, 0, Stepper_Part::D); // D-Achse referenzieren

        mode = Mode::Suche_rev;
        //message(F("Suche Messobjektende ..."), F("Nächstes x, suche Messobjektende ..."), MESSAGE_TYPE_INFO);
      }
      break;

    default:
      break;
    }

    PRINT_RST(last_measure);

    if (check_state(x)) // Stoptaster
      return false;

    if (stepperMoveTo(x, d)) {
      Serial.println(F("!Grenze überschritten!"));
    }
  } // Schleife
}

/*  ZOK - Bestimmt, ob der T-Wert einer Lasermessung unterhalb des Messobjektes liegt
    Parameter:
      x - Die zum Lasermesswertgehörige x-Position
      measure - Die aus dem Lasermesswert und der d-Position bestimmte Höhe T
    Rückgabewert:
      bool - Ist wahr, wenn der Wert innerhalb des Messobjektes liegt
*/
bool ZOK(uint_fast32_t x, uint_fast16_t d, uint_fast16_t measure) {
  if (d > AM_D_Bot_S || (AM_mirrormode == Mirrormode::Gantry_rotated ? (x < AM_X_Max_S) : (x > AM_X_Max_S)) || (AM_mirrormode == Mirrormode::Normal ? (GETPOSSMM(measure) < 0) : (GETPOSSMM(measure) > 0))) {
    return false;
  } else {
    if (x > AM_Strut_first && x < AM_Strut_last)
      return GETPOSTMM(measure) > 0;
    else
      return GETPOSTMM(measure) > AM_MachineZ + (int16_t)block_height / 2; // /2 als Sicherheitsmage
  }
}

/**
 * Printed eine uint16_t-Variable im Format ####X in out
 * @params out Ausgabespeicher
 * @params val Zu druckende Variable
 * @returns Neues Ende des Ausgabespeichers auf Position des '\0'
 */
char* print_uint16(char* out, uint16_t val) {
  uint16_t start;
  if (val >= 10000)
    start = 10000;
  else if (val >= 1000)
    start = 1000;
  else if (val >= 100)
    start = 100;
  else if (val >= 10)
    start = 10;
  else
    start = 1;
  while (start) {
    *out = val / start + '0';
    val %= start;
    start /= 10;
    out++;
  }
  *out = '\0';
  return out;
}

/**
 * Printed eine float-Variable im Format 8.2f bzw. ####X.XX in out
 * @params out Ausgabespeicher
 * @params val Zu druckende Variable
 * @returns Neues Ende des Ausgabespeichers auf Position des '\0'
 */
char* print_float(char* out, float val) {
  uint16_t start;
  uint16_t val_front = val;
  if (val < 0) {
    *out = '-';
    out++;
    val = -val;
  }

  if (val_front >= 10000)
    start = 10000;
  else if (val_front >= 1000)
    start = 1000;
  else if (val_front >= 100)
    start = 100;
  else if (val_front >= 10)
    start = 10;
  else
    start = 1;
  while (start) {
    *out = val_front / start + '0';
    val_front %= start;
    start /= 10;
    out++;
  }
  *out = AM_value_decimalpoint; out++;

  start = 10;
  val_front = (val - (uint16_t)val) * 100;
  while (start) {
    *out = val_front / start + '0';
    val_front %= start;
    start /= 10;
    out++;
  }

  *out = '\0';
  return out;
}

/** Speichert den aktuellen Punkt auf der SD-Karte
 * @params Ausgabepointer
 * @params R-Koordinate
 * @params S-Koordinate
 * @params T-Koordinate
 * @params Messpunktezähler
 * @params Reservierter Speicher
 * @params Pointer auf den Speicherstart
 * @returns Neuer Ausgabepointer oder line_buffer im Falle des Überschreitens der reservierten Speichers
*/
char* saveCoord(char* line_buffer, int_fast16_t R, float S, float T, uint_fast16_t* am_points, uint_fast16_t line_width, char* line_start) {
  if (line_width - (line_buffer - line_start) < POINT_SIZE + 1) {
    Serial.println(F("!Speicherbereich für Koordinaten überschritten!"));
    return line_buffer;
  }

  if (R < 0) {
    *line_buffer = '-'; line_buffer++;
    R = -R;
  }
  line_buffer = print_uint16(line_buffer, R);

  *line_buffer = AM_value_separator; line_buffer++;

  if (S < 0) {
    *line_buffer = '-'; line_buffer++;
    S = -S;
  }
  line_buffer = print_float(line_buffer, S);

  *line_buffer = AM_value_separator; line_buffer++;

  line_buffer = print_float(line_buffer, T);

  *line_buffer = '\n'; line_buffer++;
  *line_buffer = '\0';
  *am_points = *am_points + 1;
  return line_buffer;
}

/*  Funktionen für Schrittmotoren **************************************************************************************************************************************************************************************/

/*  stepperMoveX - Bewegt die x-Achse um x mm
  Parameter:
   x - Bewegung in x Richtung in mm
   bordersafe (=true) - Bewegung nur innerhalb der definierten Grenzen (x_min, x_max)
   waituntilfinished (=true) - Gibt an, ob die Funktion so lange warten soll, bis der Schrittmotor das Ziel erreicht hat
  Rückgabewert:
   bool - Ist wahr, wenn die anzufahrende Koordinate die Grenze überschritten hat.
*/
bool stepperMoveX(int_fast16_t x, Stepper_Part sp, bool bordersafe, bool waituntilfinished) {
  return stepperMoveTo(stepperX.currentPosition() + SCALEXMTS(x), stepperD.currentPosition(), sp, bordersafe, waituntilfinished);
}

/*  stepperMoveD - Bewegt die d-Achse um d mm
   d - Bewegung in d Richtung in mm
   bordersafe (=true) - Bewegung nur innerhalb der definierten Grenzen (d_low, d_max)
   waituntilfinished (=true) - Gibt an, ob die Funktion so lange warten soll, bis der Schrittmotor das Ziel erreicht hat
  Rückgabewert:
   bool - Ist wahr, wenn die anzufahrende Koordinate die Grenze überschritten hat.
*/
bool stepperMoveD(int_fast16_t d, Stepper_Part sp, bool bordersafe, bool waituntilfinished) {
  return stepperMoveTo(stepperX.currentPosition(), stepperD.currentPosition() + SCALEDMTS(d), sp, bordersafe, waituntilfinished);
}

/*  stepperMoveTo - Bewegt die x-Achse auf die Position (x, d) in steps
  Parameter:
   x - Bewegung auf x in x-Richtung in steps, negative Werte fahren in den Endschalter
   d - Bewegung auf d in d-Richtung in steps, negative Werte fahren in den Endschalter
   bordersafe (=true) - Bewegung nur innerhalb der definierten Grenzen (x_min, x_max, d_low, d_max)
   waituntilfinished (=true) - Gibt an, ob die Funktion so lange laufen soll, bis der Schrittmotor das Ziel erreicht hat
  Rückgabewert:
   bool - Ist wahr, wenn die anzufahrende Koordinate die Grenze überschritten hat.
*/
bool stepperMoveTo(int_fast32_t x, int_fast16_t d, Stepper_Part sp, bool bordersafe, bool waituntilfinished) {
#ifdef OFFLINE_MODE
  return false;
#endif
  bool boundary = false; // ist wahr, wenn Grenzen überschritten wurden
  if (bordersafe) { // Check Borders
    boundary = (x == 0) | (d == 0) | (d > (int16_t)d_low);
    if (d > (int16_t)d_low)
      d = d_low;
  }

  if (sp == Stepper_Part::X || sp == Stepper_Part::Both) {
    if (x == 0)
      stepperX.moveTo(-1073741824);
    else
      stepperX.moveTo(x);

    while (waituntilfinished && stepperX.distanceToGo()) {
      if (EmergencyStopTriggered) {
        EmergencyStopTriggered_Handle();
        return true;
      }
      if (EndSwitchXTriggered) {
        boundary = true;
        EndSwitchXTriggered = false;
        stepperX.stop();
      } else
        stepperX.run();
    }

    if (ENDSWITCHXTRIGGERED())
      resetLimitSwitchX();
  }

  if (sp == Stepper_Part::D || sp == Stepper_Part::Both) {
    if (d == 0)
      stepperD.moveTo(-1073741824);
    else
      stepperD.moveTo(d);

    while (waituntilfinished && stepperD.distanceToGo()) {
      if (EmergencyStopTriggered) {
        EmergencyStopTriggered_Handle();
        return true;
      }
      if (EndSwitchDTriggered) {
        boundary = true;
        break;
      } else
        stepperD.run();
    }

    if (EndSwitchDTriggered)
      resetLimitSwitchD();
  }

  return boundary;
}

/*  direct_move_control - Ermöglicht dem Bediener die direkte Steuerung X- und D-Achse
  Parameter:
   bordersafe (=true) - Bewegung nur innerhalb der definierten Grenzen (x_min, x_max, d_low, d_max)
  Rückgabewert:
   -
*/
void direct_move_control(bool bordersafe) {
  uint16_t SchrittweiteX = 1000; // in mm
  uint16_t SchrittweiteD = 100; // in mm
  bool MoveX = true; // Wenn wahr, dann x-Achse ansonsten D-Achse

  uint8_t TastenStatus = SwitchSelect - 1;

  laser_activate();

  Serial.println(F("Achsen bewegen (Angaben in mm).\nBeispiel für Bewegung der x-Achse um -10mm: x-10\nBeispiel für Bewegung der x-Achse um 100mm und der d-Achse um -25mm: x100d-25\nBeispiel für Bewegung der x-Achse um 10 Steps und der d-Achse um -25 Steps: sx100d-25\nSchließen mit q(uit)"));

  do {
    do {
      lcd.clear();
      char line[16];
      if (SchrittweiteX)
        sprintf(line, "R: %5d (%4d)%c", GETPOSRMM(), SchrittweiteX, MoveX ? '<' : ' ');
      else
        sprintf(line, "R: %5d (Step)%c", GETPOSRMM(), MoveX ? '<' : ' ');
      lcd.setCursor(0, 0);
      lcd.print(line);
      Serial.println(line);
      if (SchrittweiteD)
        sprintf(line, "D: %5d (%4d)%c", GETPOSDMM(), SchrittweiteD, MoveX ? ' ' : '<');
      else
        sprintf(line, "D: %5d (Step)%c", GETPOSDMM(), MoveX ? ' ' : '<');
      lcd.setCursor(0, 1);
      lcd.print(line);
      Serial.println(line);
      stepperX.run();
      stepperD.run();
    } while (stepperX.distanceToGo() || stepperD.distanceToGo());

    while ((TastenStatus = Tasterstatus()) == SwitchNone)
      if (EmergencyStopTriggered)
        EmergencyStopTriggered_Handle();

    if (TastenStatus & SwitchSourceSerial) {
      uint32_t starttime = millis();
      int16_t movement[2] = { 0, 0 }; // x, d
      bool negative[2] = { false, false };
      uint8_t selected = 0; // 0 => x, 1 => d
      bool steps[2] = { false, false };
      char c = last_read_char;
      do {
        if (c == 'q')
          return;
        else if (c == 's')
          steps[selected] = true;
        else if (c == 'd')
          selected = 1;
        else if (c == 'x')
          selected = 0;
        else if (c >= '0' && c <= '9')
          movement[selected] = movement[selected] * 10 + (c - '0');
        else if (c == '-')
          negative[selected] = true;
        else if (c == '\n')
          break;

        while (!Serial.available() && (millis() - starttime < 1000));

        c = Serial.read();
      } while (c != '\n' && (millis() - starttime < 1000));
      if (movement[0] != 0 || movement[1] != 0)
        stepperMoveTo(stepperX.currentPosition() + (negative[0] ? -1 : 1) * (steps[0] ? movement[0] : SCALEXMTS(movement[0])), stepperD.currentPosition() + (negative[1] ? -1 : 1) * (steps[1] ? movement[1] : SCALEDMTS(movement[1])), Stepper_Part::Both, bordersafe, true);
    } else { // Keine serielle Eingabe
      switch (TastenStatus & SwitchFilter) {
      case SwitchTop:
        if (MoveX)
          SchrittweiteX = SchrittweiteX == 0 ? 1000 : SchrittweiteX / 10;
        else
          SchrittweiteD = SchrittweiteD == 0 ? 1000 : SchrittweiteD / 10;
        break;
      case SwitchRight:
        if (MoveX) {
          if (SchrittweiteX == 0)
            stepperMoveTo(stepperX.currentPosition() + 1, stepperD.currentPosition(), Stepper_Part::Both, bordersafe, true);
          else
            stepperMoveX(SchrittweiteX, Stepper_Part::Both, bordersafe, true);
        } else {
          if (SchrittweiteD == 0)
            stepperMoveTo(stepperX.currentPosition(), stepperD.currentPosition() + 1, Stepper_Part::Both, bordersafe, true);
          else
            stepperMoveD(SchrittweiteD, Stepper_Part::Both, bordersafe, true);
        }
        break;
      case SwitchBottom:
        MoveX = !MoveX;
        break;
      case SwitchLeft:
        if (MoveX) {
          if (SchrittweiteX == 0)
            stepperMoveTo(stepperX.currentPosition() - 1, stepperD.currentPosition(), Stepper_Part::Both, bordersafe, true);
          else
            stepperMoveX(-SchrittweiteX, Stepper_Part::Both, bordersafe, true);
        } else {
          if (SchrittweiteD == 0)
            stepperMoveTo(stepperX.currentPosition(), stepperD.currentPosition() - 1, Stepper_Part::Both, bordersafe, true);
          else
            stepperMoveD(-SchrittweiteD, Stepper_Part::Both, bordersafe, true);
        }
        break;
      case SwitchMenu:
        laser_activate();
        break;
      }
    }
  } while (TastenStatus != SwitchSelect);
}

/*  resetLimitSwitchD - Fährt die D-Achse so lange zurück, bis der End-Schalter wieder gelöst ist
    Parameter:
      -
    Rückgabewert:
      -
*/
void resetLimitSwitchD() {
#ifdef OFFLINE_MODE
  return;
#endif
  delay(100);
  stepperD.setCurrentPosition(0);
  while (ENDSWITCHDTRIGGERED()) {
    stepperD.move(1);
    stepperD.run();
  }
  EndSwitchDTriggered = false;
  stepperD.setCurrentPosition(0);
}

/*  resetLimitSwitchX - Fährt die X-Achse so lange zurück, bis der End-Schalter wieder gelöst ist und setzt die Position zurück
    Parameter:
    -
    Rückgabewert:
    -
*/
void resetLimitSwitchX() {
#ifdef OFFLINE_MODE
  return;
#endif
  bool left = stepperX.currentPosition() < 500; // Theoretisch könnte auch über den rechten Endanschlag referenziert werden, dafür müsste dieser allerdings exakt vermessen werden
  if (left)
    stepperX.setCurrentPosition(0);
  while (ENDSWITCHXTRIGGERED()) {
    stepperX.move(left ? 1 : -1);
    stepperX.run();
  }
  EndSwitchXTriggered = false;
  if (left)
    stepperX.setCurrentPosition(0);
  // TODO? beim Ausfahren am Rechten Ende currentPosition auf ausgemessenen Wert setzen?
}

/*  referenceAxis - Fährt die X-Achse und die D-Achse bis an den Limit-Schalter und anschließend so lange zurück, bis der Schalter wieder gelöst ist und kalibriert dabei die x-Achse
    Parameter:
    -
    Rückgabewert:
    -
*/
void referenceAxis() {
  // X-Achse
  stepperX.setCurrentPosition(0);
  if (ENDSWITCHXTRIGGERED() && message(F("X-Limit-Schalter gedr\xF5""ckt. Befindet sich Gantry am linken Ende?"), F("X-Limit-Schalter gedrückt. Befindet sich die Gantry am linken Ende?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_NO) // Userabfrage, falls der Endschalter bereits gedrückt war, um herauszufinden, auf welcher Seite die Gantry sich befindet
  {
    stepperX.setCurrentPosition(10000);
    resetLimitSwitchX();
  }

  stepperMoveTo(0, 0, Stepper_Part::Both, false);
}

/*  isr_EndSwitchD - Interrupt-Routine beim Auslösen des Endlagenschalters der D-Achse
    Parameter:
    -
    Rückgabewert:
    -
*/
void isr_EndSwitchD() {
  EndSwitchDTriggered = true;
}

/*  isr_EndSwitchX - Interrupt-Routine beim Auslösen des Endlagenschalters der X-Achse
    Parameter:
    -
    Rückgabewert:
    -
*/
void isr_EndSwitchX() {
  EndSwitchXTriggered = true;
}


/*  Menüfunktionen  ***************************************************************************************************************************/


/*  set_filename - Setzt einen neuen Dateinamen für eine automatische Messung
    Parameter:
    -
    Rückgabewert:
    -
*/
void set_filename() {
  Serial.print(F("Bisheriger Dateiname: '"));
  Serial.print(AM_filename);
  Serial.print(F("'"));
  if (message(F("Dateinamen \xE1 ndern?"), F(" Dateinamen ändern?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_NO)
    return;
  Serial.println(F("Dateinamen ohne Dateiendung (maximal 7 Zeichen!) eingeben:"));
  Serial.readString(); // Buffer leeren
  lcd_print(F("D.name einlesen "), F("\xF5 ber Konsole    "));
  while (!Serial.available());
  String str = Serial.readString();
  str[str.length() - 1] = '\0';
  if (str.length() > 8) {
    Serial.print(F("Eingebener Dateiname zu lang!\n"));
    return;
  }

  Serial.print(F("Neuer Dateiname: '"));
  Serial.print(str);
  Serial.println(F("'"));
  strcpy(AM_filename, str.c_str());
  EEPROM.put(eeprom_AM_filename, AM_filename);
}

/** Setzt die Positionen der Höhe der mittleren Streben
*/
void set_struts_height() {
  message(F("Ger\xE1t an hoechsten Punkt der Strebe fahren"), F("Gerät an höchsten Punkt der Strebe verfahren."), MESSAGE_TYPE_CONFIRMATION);
  uint8_t response;
  // Referenzpunkt setzen
  do {
    direct_move_control(); // Laser zum Referenzpunkt verfahren
  } while ((response = message(F("Punkt setzen?"), F("Punkt setzen?"), MESSAGE_TYPE_YESNOBACK)) == MESSAGE_RESPONSE_NO);
  if (response == MESSAGE_RESPONSE_BACK)
    return;

  uint16_t measurement;
  if (!manual_measure_int(&measurement)) {
    Serial.println(F("Messung fehlgeschlagen!"));
    return;
  }

  Serial.print(F("Gemessener Wert: ")); Serial.print(measurement); Serial.println(F(" mm"));
  int16_t T = GETPOSTMM(measurement);
  Serial.print(F("T: ")); Serial.print(T); Serial.println(F(" mm"));
  if ((response = message(F("T speichern?"), F("T-Wert als Strebenhöhe abspeichern?"), MESSAGE_TYPE_YESNOBACK)) != MESSAGE_RESPONSE_YES)
    return;

  AM_MachineZ = T;

  if ((response = message(F("Fest speichern?"), F("Im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK)) == MESSAGE_RESPONSE_YES) {
    EEPROM.put(eeprom_AM_MachineZ, AM_MachineZ);
  }
}


/*  Set_Machine_Reference - Referenziert die Maschine am Referenzpunkt, dem Punkt am Boden unter dem Nullpunkt
    Parameter:
     serial (=false) - True, wenn serieller Eingabemodus (Bestätigungs-Nachrichten werden auf LCD nicht angezeigt)
    Rückgabewert:
     bool - Referenz gesetzt
*/
bool Set_MObject_Reference(bool serial) {
  Serial.print(F("Der Referenzpunkt befindet sich direkt unter dem Ursprung des zu messenden Objektes in der Regel auf dem Boden.\n"));
  char msg[200];
  uint32_t e_x; EEPROM.get(eeprom_Ref_PosXStep, e_x);
  uint32_t e_d; EEPROM.get(eeprom_Ref_PosDStep, e_d);
  uint32_t e_h; EEPROM.get(eeprom_Ref_PosMMM, e_h);
  sprintf(msg, "Temp gespeicherter Referenzpunkt (EEPROM): x = %d mm (%d mm), d = %u mm (%u mm), Hoehe = %u mm (%u mm)\n", (int32_t)SCALEXSTM(Ref_PosXStep), (int32_t)SCALEXSTM(e_x), (uint16_t)SCALEDSTM(Ref_PosDStep), (uint16_t)SCALEDSTM(e_d), (uint16_t)Ref_PosMMM, (uint16_t)e_h);
  Serial.print(msg);
  uint8_t ans = message(F("Soll der gespeicherte Referenzpunkt angefahren werden?"), F("Gerät an gespeicherten Referenzpunkt verfahren."), MESSAGE_TYPE_YESNOBACK);
  if (ans == MESSAGE_RESPONSE_BACK)
    return false;
  else if (ans == MESSAGE_RESPONSE_YES) {
    stepperMoveTo(Ref_PosXStep, Ref_PosDStep);
  }

  if (message(F("Neuen Referenzpunkt setzen?."), F("Neuen Referenzpunkt setzen?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_NO)
    return false;

  message(F("Ger\xE1t an Referenzpunkt verfahren."), F("Gerät an Referenzpunkt (auf Boden) verfahren."), MESSAGE_TYPE_CONFIRMATION);

  // Referenzpunkt setzen
  do {
    direct_move_control(); // Laser zum Referenzpunkt verfahren
  } while (!serial && (ans = message(F("RefPunkt setzen?"), F("Referenzpunkt setzen?"), MESSAGE_TYPE_YESNO)) == MESSAGE_RESPONSE_NO);
  if (ans == MESSAGE_RESPONSE_BACK)
    return false;

  uint16_t measurevalue;
  while (1) {
    message(F("Setze Referenz: messe Distanz..."), F("Messe Distanz und setze sie als Referenz..."));
    if (manual_measure_averaged(&measurevalue, 5, 1, true)) {
      break;
    } else {
      if (message(F("Messung fehlgeschlagen, Vorgang abgebrochen!"), F("Distanzmessung fehlgeschlagen, Messung wiederholen?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_NO) {
        if (message(F("Messung abgebrochen, Manuelle Eingabe?"), F("Messung abgebrochen, Manuelle Eingabe des Referenzwertes?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_YES) {
          if (!get_value(F("Ref-Wert eingeben"), F("Referenzwert in mm eingeben: "), &measurevalue, measurevalue, 2000)) {
            message(F("Eingabe fehlgeschlagen, Abbruch!"), F("Referenzwerteingabe fehlgeschlagen, Vorgang abgebrochen!"), MESSAGE_TYPE_CONFIRMATION);
            return false;
          } else {
            break;
          }
        } else { // Messung abbrechen
          return false;
        }
      }
    }
  }

  Serial.print(F("Gemessene Distanz: "));
  Serial.print(measurevalue);
  Serial.println(F(" mm"));

  Ref_PosMMM = measurevalue;
  Serial.print(F("Ref_PosMMM = "));
  Serial.println(Ref_PosMMM);
  Ref_PosDStep = stepperD.currentPosition();
  Ref_PosXStep = stepperX.currentPosition();

  // Eingabe der Höhe
  laser_activate();
  uint16_t height;
  if (!get_value(F("H\xEFhe in mm:"), F("Höhe in mm eingeben: "), &height))
    return false;

  Ref_Height = height;
  Serial.println(Ref_Height);

  message(F("Referenzpunkt gesetzt!"), F("Referenzpunkt gesetzt!"), serial ? MESSAGE_TYPE_INFO : MESSAGE_TYPE_CONFIRMATION);

  uint8_t response;
  if ((response = message(F("Fest speichern?"), F("Im EEPROM speichern?"), MESSAGE_TYPE_YESNO)) == MESSAGE_RESPONSE_YES) {
    EEPROM.put(eeprom_Ref_PosMMM, Ref_PosMMM);
    EEPROM.put(eeprom_Ref_PosDStep, Ref_PosDStep);
    EEPROM.put(eeprom_Ref_PosXStep, Ref_PosXStep);
    EEPROM.put(eeprom_Ref_Height, Ref_Height);
    message(F("Referenz im EEPROM gespeichert!"), F("Referenz im EEPROM gespeichert"), MESSAGE_TYPE_CONFIRMATION);
  }
  return true;
}

/*  set_start_position - Setzt die Startposition für die automatische Messung
    Parameter:
     serial (=false) - True, wenn serieller Eingabemodus (Bestätigungs-Nachrichten werden auf LCD nicht angezeigt)
    Rückgabewert:
     serial
*/
bool set_start_position(bool serial) {
  Serial.print(F("Der Startpunkt ist der erste Punkt der auf dem Messobjekt angefahren wird.\n"));
  char msg[200];
  uint32_t e_x; EEPROM.get(eeprom_AM_X_Start_S, e_x);
  uint16_t e_d; EEPROM.get(eeprom_AM_D_Start_S, e_d);
  sprintf(msg, "Temp gespeicherter Startpunkt (EEPROM): x = %d mm (%d mm), d = %u mm (%u mm)\n", GETPOSRMM(AM_X_Start_S), GETPOSRMM(e_x), (uint16_t)SCALEDSTM(AM_D_Start_S), (uint16_t)SCALEDSTM(e_d));
  Serial.print(msg);
  uint8_t ans = message(F("Soll der gespeicherte Startpunkt angefahren werden?"), F("Gerät an gespeicherten Startpunkt verfahren?"), MESSAGE_TYPE_YESNOBACK);
  if (ans == MESSAGE_RESPONSE_BACK)
    return false;
  else if (ans == MESSAGE_RESPONSE_YES) {
    stepperMoveTo(AM_X_Start_S, AM_D_Start_S);
  }

  if (message(F("Neuen Startpunkt setzen?."), F("Neuen Startpunkt setzen?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_NO)
    return false;

  message(F("Ger\xE1t an Startpunkt verfahren."), F("Startpunkt setzen: Den obersten Punkt in der ersten x-Position anfahren!"), serial ? MESSAGE_TYPE_INFO : MESSAGE_TYPE_CONFIRMATION);
  uint8_t response;

  do {
    direct_move_control();
  } while ((response = message(F("Startpunkt setzen?"), F("Startpunkt setzen?"), MESSAGE_TYPE_YESNOBACK)) == MESSAGE_RESPONSE_NO);
  if (response == MESSAGE_RESPONSE_BACK)
    return serial;

  AM_D_Start_S = stepperD.currentPosition();
  AM_X_Start_S = stepperX.currentPosition();

  if ((message(F("Fest speichern?"), F("Im EEPROM speichern?"), MESSAGE_TYPE_YESNO)) == MESSAGE_RESPONSE_YES) {
    EEPROM.put(eeprom_AM_D_Start_S, AM_D_Start_S);
    EEPROM.put(eeprom_AM_X_Start_S, AM_X_Start_S);
    message(F("Startpunkt im EEPROM gespeichert!"), F("Startpunkt im EEPROM gespeichert"), MESSAGE_TYPE_CONFIRMATION);
  }
  return serial;
}

/*  EmergencyStopTriggered_Handle - Handelt nach einem Notstop die Referenzierung der Achsen
    Parameter:
    -
    Rückgabewert:
    -
*/
void EmergencyStopTriggered_Handle() {
  lcd.begin(16, 2);
  message(F("24V offline!"), F("24V-Spannungsversorgung inaktiv! Spannungsversorgung aktivieren!"), MESSAGE_TYPE_CONFIRMATION);
  message(F("Referenziere Achsen..."), F("Referenziere Achsen..."), MESSAGE_TYPE_INFO);
  uint32_t d = stepperD.currentPosition();
  uint32_t x = stepperX.currentPosition();
  while (EMERGENCYSTOPTRIGGERED);
  EmergencyStopTriggered = false;
  referenceAxis();
  if (message(F("Letzte Position anfahren?"), F("Letzte bekannte Position anfahren?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_YES)
    stepperMoveTo(x, d);
}

/*  isr_EmergencyStop - Interrupt-Routine beim Auslösen des Notstops
  * Parameter:
  * -
  * Rückgabewert:
  * -
  */
void isr_EmergencyStop() {
  EmergencyStopTriggered = true;
}

/*  isr_Stop - Interrupt-Routine beim Auslösen des Stop-Tasters
    Parameter:
      -
    Rückgabewert:
      -
*/
ISR(PCINT2_vect) {
  state = STATE_CLICKED;
}


void isr_Stop() {
  if (message(F("Notstop! Betrieb fortfahren?"), F("\nNotstop! Betrieb fortfahren?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_YES) {
    state = STATE_NORMAL;
    return;
  } else {
    uint8_t menu_position = 0;
    const uint8_t MENU_POSITIONS_STOP = 2;
    const __FlashStringHelper* options[MENU_POSITIONS_STOP + 1]{
      F("Fortfahren"),
      F("Stop"),
      F("----------------") };
    bool newSerial = true;

    while (1) {
      if (newSerial) {
        Serial.println(F("x - senden, um Menüeintrag x auszuwählen"));
        for (uint8_t i = 0; i < MENU_POSITIONS_STOP; i++) {
          Serial.print(i);
          Serial.print(F(" - "));
          Serial.println(options[i]);
        }

        newSerial = false;
      }
      lcd_print(options[menu_position], options[menu_position + 1]);
      lcd.setCursor(15, 0);
      lcd.print(F("<"));

      uint8_t TastenStatus;
      while ((TastenStatus = Tasterstatus()) == SwitchNone); // Warten auf Tasterinput

      if (TastenStatus & SwitchSourceSerial)
        if ((newSerial = AM_Menu_Check(last_read_char - '0')))
          return;

      switch (TastenStatus & SwitchFilter) {
      case SwitchTop:
        menu_position = menu_position ? menu_position - 1 : MENU_POSITIONS - 1;
        break;
      case SwitchRight:
        if (AM_Menu_Check(menu_position))
          return;
        break;
      case SwitchBottom:
        menu_position = menu_position < MENU_POSITIONS - 1 ? menu_position + 1 : 0;
        break;
      case SwitchLeft:
        break;
      }
    }
  }
}

/*  AM_Menu_Check - Führt die Aktion zum angewählten Menüeintrag aus
    Parameter:
      index - Den angewählten Menüeintrag
    Rückgabewert:
      bool - True, wenn Menueintrag ausgewählt
*/
bool AM_Menu_Check(uint8_t index) {
  switch (index) {
  case 0: // Fortfahren
    state = STATE_NORMAL;
    return true;
  case 1: // Stop
    state = STATE_STOP;
    return true;
  default:
    return false;
  }
  return false;
}

/*  check_state - Prüft, ob der Stoptaster gedrückt wurde und handelt entsprechend
    Parameter:
      x - Aktuelle x-Position
    Rückgabewert:
      bool - True, wenn Ausführung der Messung abgebrochen werden soll
*/
bool check_state(uint32_t x) {
  if (state == STATE_CLICKED || Serial.available() > 1) {
    Serial.readString();
    isr_Stop();
    switch (state) {
    case STATE_STOP:
      return true;
    }
  }
  return false;
}

/****************************************************************/
// MENU
uint8_t SpecialCommand(char c) {
  switch (c) {
  case 'l': { //MENUL
    Serial.println(F("Messe Distanz..."));
    lcd_print(F("Messe Distanz..."), F(""));
    char value[5];
    if (manual_measure(value)) {
      Serial.print(F("Distanz: "));
      Serial.print(value);
      Serial.println(F(" mm"));
      lcd_print(F("Distanz:"), F(""));
      lcd.setCursor(0, 1);
      lcd.print(value);
      lcd.print(F(" mm"));
      uint16_t m;
      sscanf(value, "%d", &m);
      PRINT_RST(m);
    } else {
      Serial.println(F("Messung fehlgeschlagen!"));
    }
    return SwitchNone;
  }
  case 'i':
    Serial.println(SERIALINFOTEXT);
    return SwitchNone;
  case 'o':
    laser_activate();
    return SwitchNone;
  case 'm':
    return SwitchMenu | SwitchSourceSerial;
  case 'p': {
    uint16_t measure;
    if (manual_measure_int(&measure)) {
      laser_activate();
      float S = GETPOSSMM(measure);
      float T = GETPOSTMM(measure);
      char RA[10], SA[10], TA[10];
      dtostrf(GETPOSRMM(), 9, 1, RA);
      dtostrf(S, 9, 1, SA);
      dtostrf(T, 9, 1, TA);
      Serial.print(F("ZOK: "));
      if (ZOK(GETPOSXSTEP(), GETPOSDSTEP(), measure))
        Serial.print(F("Treffer"));
      else
        Serial.print(F("Daneben, "));
      Serial.print(F("m: ")); Serial.print(measure); Serial.print(F(" R: ")); Serial.print(RA); Serial.print(F(" S: ")); Serial.print(S); Serial.print(F(" T: ")); Serial.println(T);
      Serial.print(F("PosRefD  Step: ")); Serial.print(Ref_PosDStep); Serial.print(F(" Height: ")); Serial.println(Ref_Height);
    } else
      Serial.println(F("Messung fehlgeschlagen!"));
    return SwitchSourceSerial;
  }
  }
  return SwitchSourceSerial;
}

bool move_function(uint32_t* x, const uint16_t eeprom_x, uint16_t* d, const uint16_t eeprom_d, Stepper_Part sp, const __FlashStringHelper* hint) {
  Serial.println(hint);

  uint32_t e_x;
  uint16_t e_d;

  Serial.println(F("Gespeicherter Punkt (steps, EEPROM steps)"));
  if (sp == Stepper_Part::X || sp == Stepper_Part::Both) {
    EEPROM.get(eeprom_x, e_x);
    Serial.print(F("X = ")); Serial.print(SCALEXSTM(*x)); Serial.print(F(" mm (")); Serial.print(*x); Serial.print(F(", ")); Serial.print(e_x); Serial.println(F(")"));
  }
  if (sp == Stepper_Part::D || sp == Stepper_Part::Both) {
    EEPROM.get(eeprom_d, e_d);
    Serial.print(F("D = ")); Serial.print(SCALEXSTM(*d)); Serial.print(F(" mm (")); Serial.print(*d); Serial.print(F(", ")); Serial.print(e_d); Serial.println(F(")"));
  }

  while (1) {
    Serial.println(F("1 - Gespeicherten Punkt anfahren\n2 - EEPROM Punkt anfahren\n3 - Wert(e) manuell anfahren\n4 - Wert(e) eingeben\n5 - zurück\n"));
    uint16_t res;
    if (!get_value(F("USB"), F("Wähle mit 1 - 5"), &res, 1, 5))
      return false;

    switch (res) {
    case 1:
      stepperMoveTo(*x, *d, sp);
      break;
    case 2:
      stepperMoveTo(e_x, e_d, sp);
      break;
    case 3:
      Serial.println(F("Punkt anfahren"));
      uint8_t response;
      do {
        direct_move_control();
      } while ((response = message(F("Wert setzen?"), F("Wert setzen?"), MESSAGE_TYPE_YESNOBACK)) == MESSAGE_RESPONSE_NO);

      if (response == MESSAGE_RESPONSE_BACK)
        return false;

      if (sp == Stepper_Part::X || sp == Stepper_Part::Both)
        *x = stepperX.currentPosition();
      if (sp == Stepper_Part::D || sp == Stepper_Part::Both)
        *d = stepperD.currentPosition();

      if (message(F("Wert im EEPROM speichern?"), F("Wert im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK) == MESSAGE_RESPONSE_YES) {
        if (sp == Stepper_Part::X || sp == Stepper_Part::Both)
          EEPROM.put(eeprom_x, *x);
        if (sp == Stepper_Part::D || sp == Stepper_Part::Both)
          EEPROM.put(eeprom_d, *d);
      }
      return true;
    case 4:
      if (sp == Stepper_Part::X || sp == Stepper_Part::Both) {
        uint16_t x_temp;
        if (!get_value(F("X-Wert eingeben (mm)"), F("X-Wert eingeben (mm)"), &x_temp))
          break;
        *x = SCALEXMTS(x_temp);
        if (message(F("Wert im EEPROM speichern?"), F("Wert im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK) == MESSAGE_RESPONSE_YES)
          EEPROM.put(eeprom_x, *x);
      }
      if (sp == Stepper_Part::D || sp == Stepper_Part::Both) {
        uint16_t d_temp;
        if (!get_value(F("D-Wert eingeben (mm)"), F("D-Wert eingeben (mm)"), &d_temp))
          break;
        *d = SCALEXMTS(d_temp);
        if (message(F("Wert im EEPROM speichern?"), F("Wert im EEPROM speichern?"), MESSAGE_TYPE_YESNOBACK) == MESSAGE_RESPONSE_YES)
          EEPROM.put(eeprom_d, *d);
      }
      return true;
    default:
      return false;
    }
  }
}

/**
 * @brief Reboots the whole system
 */
void reboot() {
  Serial.print(F("Starte neu...\n"));
  wdt_enable(WDTO_2S);
  while (1);
}

/****************************************************************/
// Tests

void X_Axis_test() {
  Serial.println(F("\nStarte X-Achsen-Test\nReferenziere Achsen"));
  referenceAxis();
  Serial.println(F("Fahre zum Ende der X-Achse"));

  stepperX.move(10000000);

  while (!ENDSWITCHXTRIGGERED())
    stepperX.run();

  int32_t x_max = stepperX.currentPosition();
  Serial.println(F("Ende gefunden, fahre zurück zum Start\n"));

  stepperX.move(-10000000);

  while (ENDSWITCHXTRIGGERED())
    stepperX.run();
  while (!ENDSWITCHXTRIGGERED())
    stepperX.run();

  int32_t x = stepperX.currentPosition();

  Serial.print(F("Maximales X: ")); Serial.print(SCALEXSTM(x_max)); Serial.print(F(" mm (")); Serial.print(x_max); Serial.println(F(")"));
  Serial.print(F("Delta X: ")); Serial.print(SCALEXSTM(x)); Serial.print(F(" mm (")); Serial.print(x); Serial.println(F(")"));
  Serial.print(F("Stepverlust: ")); Serial.print(x * 100.0 / x_max, 4); Serial.println(" % (Delta/X_Max)");

  stepperX.move(10000000);
  while (ENDSWITCHXTRIGGERED())
    stepperX.run();
  referenceAxis();
}

void D_Axis_test() {
  Serial.println(F("\nStarte D-Achsen-Test\nReferenziere Achsen"));
  referenceAxis();
  Serial.println(F("Fahre zum Ende der D-Achse"));

  stepperD.move(d_low);

  while (stepperD.currentPosition() < d_low)
    stepperD.run();

  int32_t d_max = stepperD.currentPosition();
  Serial.println(F("Ende gefunden, fahre zurück zum Start\n"));

  stepperD.move(-10000000);

  while (!ENDSWITCHDTRIGGERED())
    stepperD.run();

  int32_t d = stepperD.currentPosition();

  Serial.print(F("Maximales D: ")); Serial.print(SCALEDSTM(d_max)); Serial.print(F(" mm (")); Serial.print(d_max); Serial.println(F(")"));
  Serial.print(F("Delta D: ")); Serial.print(SCALEDSTM(d)); Serial.print(F(" mm (")); Serial.print(d); Serial.println(F(")"));
  Serial.print(F("Stepverlust: ")); Serial.print(d * 100.0 / d_max, 4); Serial.println(" % (Delta/D_Max)");

  referenceAxis();
}

void Length_test() {
  Serial.println(F("\nStarte Längen-Test\nFahre linke Grenze an..."));

  stepperX.move(-10000000);
  while (!ENDSWITCHXTRIGGERED())
    stepperX.run();

  stepperX.stop();
  while (stepperX.distanceToGo())
    stepperX.run();

  while (ENDSWITCHXTRIGGERED()) {
    stepperX.move(1);
    stepperX.run();
  }

  uint32_t startpos = stepperX.currentPosition();
  if (message(F("Rechte Grenze anfahren?"), F("Rechte Grenze anfahren?"), MESSAGE_TYPE_YESNOBACK) != MESSAGE_RESPONSE_YES)
    return;

  stepperX.move(10000000);
  while (!ENDSWITCHXTRIGGERED())
    stepperX.run();

  stepperX.stop();
  while (stepperX.distanceToGo())
    stepperX.run();

  while (ENDSWITCHXTRIGGERED()) {
    stepperX.move(-1);
    stepperX.run();
  }

  startpos = stepperX.currentPosition() - startpos;
  Serial.print(F("Gefahrene Distanz: ")); Serial.print(SCALEXSTM(startpos)); Serial.print(F(" mm (")); Serial.print(startpos); Serial.println(F(" steps)"));

  if (message(F("Test abgeschlossen, Referenzieren?"), F("Test abgeschlossen, System referenzieren?"), MESSAGE_TYPE_YESNO) == MESSAGE_RESPONSE_YES)
    referenceAxis();
}

void Precision_test() {
  Serial.println(F("\nPräzisions-Test\n1. Gerät manuell an einen beliebigen Startpunkt verfahren\n2. Punkt auf Boot markieren\n3. Zeit (in Sekunden) angeben, die der Test dauern soll\n4. Der Laser simuliert einen automatischen Test so lange wie angegeben\n5. Der Laser verfährt zurück an den Startpunkt, der Laserpunkt sollte mit dem aufgemalten Punkt in Deckung sein!"));

  do {
    direct_move_control();
  } while ((message(F("Startpunkt wählen?"), F("Startpunkt wählen?"), MESSAGE_TYPE_YESNOBACK)) == MESSAGE_RESPONSE_NO);

  uint32_t x_start = GETPOSXSTEP();
  uint16_t d_start = GETPOSDSTEP();
  uint32_t x = x_start;
  uint16_t d = d_start;

  uint16_t time;
  if (!get_value(F("Wie viele Sekunden soll verfahren werden?"), F("Wie viele Sekunden soll verfahren werden?"), &time))
    return;

  Serial.println(F("Starte Test..."));
  uint32_t starttime = millis();
  uint32_t endtime = starttime + (uint32_t)time * 1000;
  while (millis() < endtime) {
    if (d > AM_D_Bot_S) {
      d = d_start;
      x += AM_Inkrement_X_G;
      if (x > AM_X_Max_S) {
        Serial.println(F("Ende der X-Achse erreicht, Testende"));
        break;
      }
    } else if (d < x_start + AM_Amount_Smallloops_D * AM_Inkrement_D_K || d > AM_D_Bot_S - AM_Amount_Smallloops_D * AM_Inkrement_D_K)
      d += AM_Inkrement_D_K;
    else
      d += AM_Inkrement_D_G;
    stepperMoveTo(x, d);
  }

  stepperMoveTo(x_start, d_start);

  Serial.println(F("Startposition erneut angefahren, Differenz manuell bestimmen"));
  Serial.println(F("Test abgeschlossen!"));
}