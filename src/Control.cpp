#include "Control.h"
#include <Arduino.h>

uint8_t last_switch_value = 0;
uint32_t last_switch_click = 0;
bool switch_down = false;

LiquidCrystal lcd(8, 9, 4, 5, 6, 7); //Angabe der erforderlichen Pins;

uint8_t(*_SpecialCommand)(char c);
char last_read_char;


uint8_t Tasterstatus() {
  last_read_char = '\0';
  if (Serial.available() > 0) {
    char c = Serial.read();
    last_read_char = c;
    if (c == '#') {
      uint32_t starttime = millis();
      while (!Serial.available() && (millis() - starttime < 1000));
      c = Serial.read();
      _SpecialCommand(c);
      return SwitchNone;
    }
    return SwitchSourceSerial;
  }

  uint16_t Analogwert = analogRead(A0); // Auslesen der Taster am Analogen Pin A0.

  if (Analogwert >= 790 && digitalRead(ExternalSwitchPin)) { // Kein Taster gedrückt
    if (millis() - last_switch_click > 300) {
      last_switch_click = millis();
      last_switch_value = SwitchNone;
    }
    return SwitchNone;
  } else { // Taster gedrückt
    if (millis() - last_switch_click > 300) { // Entprellzeit abwarten
      if ((last_switch_value & SwitchFilter) != SwitchNone) { // (ein) Taster war bereits gedrückt
        uint8_t switch_value = SwitchNone;
        if (Analogwert < 50) switch_value = SwitchRight;
        else if (Analogwert < 195) switch_value = SwitchTop;
        else if (Analogwert < 380) switch_value = SwitchBottom;
        else if (Analogwert < 555) switch_value = SwitchLeft;
        else if (Analogwert < 790) switch_value = SwitchSelect;
        switch_value |= SwitchSourceFront;

        if (switch_value == last_switch_value || digitalRead(ExternalSwitchPin)) {
          if (millis() - last_switch_click > 1500) { // Gedrückt halten
            last_switch_click = millis() - 1250; // last_switch_click überschreiben, um ein zu schnelles Klicken zu verhindern
            if (digitalRead(ExternalSwitchPin))
              switch_value |= SwitchSourceExternalSwitch;
            return switch_value;
          }
        } else {
          last_switch_click = millis();
          return last_switch_value = switch_value;
        }
      } else {
        last_switch_click = millis();
        if (Analogwert < 50) return last_switch_value = SwitchRight | SwitchSourceFront;
        else if (Analogwert < 195) return last_switch_value = SwitchTop | SwitchSourceFront;
        else if (Analogwert < 380) return last_switch_value = SwitchBottom | SwitchSourceFront;
        else if (Analogwert < 555) return last_switch_value = SwitchLeft | SwitchSourceFront;
        else if (Analogwert < 790) return last_switch_value = SwitchSelect | SwitchSourceFront;
      }
    }
  }
  return SwitchNone;
}

uint8_t Tasterstatus_Filtered() {
  uint8_t result = Tasterstatus();
  //Serial.print("New Char: "); Serial.println((uint8_t)last_read_char);
  //Serial.print("Result: "); Serial.println(result);
  if (result & SwitchSourceSerial)
    switch (last_read_char) {
    case 'j': case'y':
      return SwitchRight;
      break;
    case 'n':
      return SwitchLeft;
      break;
    case 'q': case 'b':
      return SwitchSelect;
      break;
    }
  return result & SwitchFilter;
}

void lcd_print(const __FlashStringHelper* line1, const __FlashStringHelper* line2) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(line1);
  lcd.setCursor(0, 1);
  lcd.print(line2);
}

void lcd_print(const char* line1, const char* line2) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(line1);
  lcd.setCursor(0, 1);
  lcd.print(line2);
}

void lcd_print_r(const char* line1, const char* line2) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(line1);
  lcd.setCursor(0, 1);
  lcd.print(line2);
}

uint8_t message(const __FlashStringHelper* lcdText, const __FlashStringHelper* serialText, const uint8_t message_type) {
  Serial.println(serialText);
  if (message_type == MESSAGE_TYPE_CONFIRMATION)
    return false;
  else if (message_type == MESSAGE_TYPE_YESNO || message_type == MESSAGE_TYPE_YESNOBACK)
    Serial.println(F("(n)< Nein Ja >(j)"));

  lcd.clear();
  lcd.setCursor(0, 0);
  uint16_t i = 0;
  while (((const char*)lcdText)[i] != '\0' || message_type == MESSAGE_TYPE_YESNO || message_type == MESSAGE_TYPE_YESNOBACK) {
    if (((const char*)lcdText)[i] == '\0' && (message_type == MESSAGE_TYPE_YESNO || message_type == MESSAGE_TYPE_YESNOBACK)) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(F("< Nein      Ja >"));
    } else
      lcd.print(((const char*)lcdText)[i++]);

    if (i % 16 == 0) // Zeilenumbruch
      lcd.setCursor(0, 1);

    if (i % 32 == 0 || ((const char*)lcdText)[i] == '\0') {
      if (message_type == MESSAGE_TYPE_INFO)
        return MESSAGE_RESPONSE_BACK;
      else if (((const char*)lcdText)[i] == '\0' && (message_type == MESSAGE_TYPE_YESNO || message_type == MESSAGE_TYPE_YESNOBACK) && (i % 32) < 17) {
        lcd.setCursor(0, 1);
        lcd.print(F("< Nein      Ja >"));
      }
      uint8_t TasterStatus = 1;
      while (TasterStatus) {
        TasterStatus = Tasterstatus_Filtered();
        switch (TasterStatus) {
        case SwitchTop:
          if (i > 63) {
            TasterStatus = 0;
            i -= 32 + (i % 32 ? i % 32 : 32);
          }
          break;
        case SwitchBottom:
          if (((const char*)lcdText)[i] != '\0')
            TasterStatus = 0;
          break;
        case SwitchSelect:
          if (message_type == MESSAGE_TYPE_CONFIRMATION || message_type == MESSAGE_TYPE_YESNOBACK)
            return MESSAGE_RESPONSE_BACK;
          break;
        case SwitchLeft:
          return MESSAGE_RESPONSE_NO;
        case SwitchRight:
          return MESSAGE_RESPONSE_YES;
        default:
          TasterStatus = 1;
          break;
        }
      }
      lcd.clear();
      lcd.setCursor(0, 0);
    }
  }

  if (message_type != MESSAGE_TYPE_INFO)
    while (Tasterstatus_Filtered() == SwitchNone);
  return false;
}

bool get_value(const __FlashStringHelper* lcdInfotText, const __FlashStringHelper* serialText, uint16_t* result, uint16_t initial_value, uint16_t maximum) {
  uint8_t TastenStatus = SwitchSelect - 1;
  uint16_t Cursor = 1;
  Serial.println(serialText);
  Serial.print(F("Initalwert: "));
  if (initial_value != (uint16_t)-1)
    Serial.println(initial_value);
  else
    Serial.println(*result);
  lcd.blink();

  do {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(lcdInfotText);
    char line[16];
    if (initial_value != (uint16_t)-1)
      sprintf(line, "%5d", initial_value);
    else
      sprintf(line, "%5d", *result);
    lcd.setCursor(0, 1);
    lcd.print(line);
    lcd.setCursor(log10p(Cursor), 1);

    Serial.readString();
    while ((TastenStatus = Tasterstatus()) == SwitchNone);

    if (TastenStatus & SwitchSourceSerial) { // Eingabe per serielle Schnittstelle
      uint16_t value = last_read_char - '0';
      if (last_read_char == 'q')
        return false;
      char c;
      do {
        c = Serial.read();
        if (c != -1 && c >= '0' && c <= '9')
          value = value * 10 + (c - '0');
        else if (c == 'q')
          return false;
      } while (c != '\n');
      initial_value = value;
      Serial.println(value);
      break;
    }

    switch (TastenStatus & SwitchFilter) {
    case SwitchTop:
      initial_value += Cursor;
      break;
    case SwitchRight:
      if (Cursor < maximum)
        Cursor *= 10;
      break;
    case SwitchBottom:
      initial_value -= Cursor;
      break;
    case SwitchLeft:
      if (Cursor > 10)
        Cursor /= 10;
      break;
    }
  } while (TastenStatus != SwitchSelect);

  lcd.noBlink();
  *result = initial_value;
  return true;
}