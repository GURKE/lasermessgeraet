#include "Filebrowser.h"
#include <Arduino.h>

const uint_least8_t Max_JobLength = 5;
const uint_least8_t Max_ArgLength = 8;

char AM_filename[Max_Filenamelength] = { 0 };

const uint8_t AM_SD_JOBS = 7;
static bool (*jobs_fun[AM_SD_JOBS])(char**, uint_least8_t, char**) = { Filebrowser::Jobs::Q, Filebrowser::Jobs::LS, Filebrowser::Jobs::CD, Filebrowser::Jobs::MKDIR, Filebrowser::Jobs::CAT, Filebrowser::Jobs::RM, Filebrowser::Jobs::PWD };
const String jobs_name[AM_SD_JOBS] = { "q", "ls", "cd", "mkdir", "cat", "rm", "pwd" };

/*  filebrowser - Ein bedienbarer Dateibrowser
    Parameter:
      f - Das Ursprungsverzeichnis
    Rückgabewert:
      -
*/
void filebrowser() {
  Serial.readString(); // Empty cache
  Serial.println(F("Filebrowser: "));
  Serial.print(F("Hilfe: "));
  for (uint8_t i = 0; i < AM_SD_JOBS; i++) {
    Serial.print(jobs_name[i]);
    Serial.print(F(" / "));
  }
  Serial.print(F(" [target]\n\n"));
  char** dir = (char**)malloc(sizeof(*dir));
  *dir = (char*)malloc(2 * sizeof(**dir));
  (*dir)[0] = '/';
  (*dir)[1] = '\0';
  uint_least8_t i;
  File file;
  char job[Max_JobLength + 1] = "ls";
  char** arguments = {};
  uint_least8_t am_args = 0;

  while (1) {
    for (i = 0; i < AM_SD_JOBS; i++) {
      if (jobs_name[i] == job)
        break;
    }

    if ((*jobs_fun[i])(dir, am_args, arguments))
      return;

    while (!Serial.available());

    for (uint_fast8_t i = 0; i < am_args; i++)
      free(arguments[i]);

    am_args = 0;
    char c = Serial.read();
    for (uint_least8_t i = 0; i < Max_JobLength; i++) {
      job[i] = c;
      if (job[i] == ' ' || job[i] == '\n') {
        job[i] = '\0';
        break;
      }
      while (!Serial.available());
      c = Serial.read();
      //Serial.print("A '"); Serial.print(c); Serial.print(" - "); Serial.println((uint8_t)c);
    }
    uint_least8_t i = 0;
    while (c != '\n' && c != '\0') {
      if (c == ' ') {
        if (am_args) {
          arguments[am_args - 1][i] = '\0';
        }
        i = 0;
        arguments = (char**)realloc(arguments, (am_args + 1) * sizeof(*arguments));
        arguments[am_args] = (char*)malloc(Max_ArgLength * sizeof(*(arguments[0])));
        am_args++;
      } else {
        if (i > Max_ArgLength - 2) {
          arguments[am_args - 1] = (char*)realloc(arguments[am_args - 1], (i + 2) * sizeof(*(arguments[0])));
        }
        arguments[am_args - 1][i] = c;
        i++;
      }
      while (!Serial.available());
      c = Serial.read();
    }
    if (am_args)
      arguments[am_args - 1][i] = '\0';
  }
  free(*dir);
  free(dir);
}

bool Filebrowser::Jobs::Q(char** dir, uint_least8_t am_arg, char** arguments) {
  return true;
}

bool Filebrowser::Jobs::LS(char** dir, uint_least8_t am_arg, char** arguments) {
  char* path;
  if (am_arg > 0) {
    path = GetPath(*dir, arguments[0], FileType::Directory);
    if (path == nullptr) {
      Serial.println(F("Unknown Folder"));
      return false;
    }
  } else {
    path = (char*)malloc((strlen(*dir) + 1) * sizeof(*path));
    strcpy(path, *dir);
  }

  /*if (!SD.exists(path)) {
    Serial.print(F("Folder '"));
    Serial.print(path);
    Serial.print(F("' does not exist!"));
    return false;
  }*/

  Serial.print(F("Reading directory \""));
  Serial.print(path);
  Serial.println(F("\":"));

  File directory = SD.open(path);

  free(path);
  char filename[13];

  while (true) {
    File entry = directory.openNextFile();
    if (!entry) break; // no more files
    Serial.print(entry.name());
    for (uint8_t i = strlen(filename); i < 8; i++)
      Serial.print(F(" "));
    if (entry.isDirectory()) {
      Serial.print(F("\t"));Serial.print(strlen(filename));Serial.print(F("\t"));
      Serial.println(F("DIR"));
    } else { // files have sizes, directories do not
      Serial.print(F("\t"));
      Serial.print(F("FILE"));
      Serial.print(F("\t"));
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
  //directory.close();
  return false;
}

bool Filebrowser::Jobs::CD(char** dir, uint_least8_t am_arg, char** arguments) {
  if (am_arg < 1) {
    Serial.println(F("No directory name given!"));
    return false;
  }

  if (arguments[0][0] == '.' && arguments[0][1] == '.') { // ..
    uint_least8_t i;
    for (i = strlen(*dir) - 2; i > 0; i--) {
      if ((*dir)[i] == '/') {
        (*dir)[i + 1] = '\0';
        break;
      }
    }
    if (i == 0) {
      (*dir)[0] = '/';
      (*dir)[1] = '\0';
    }
  } else if (arguments[0][0] == '/') { // From Rootdirectory
    free(*dir);
    *dir = (char*)malloc((strlen(arguments[0]) + 1) * sizeof(**dir));
    strcpy(*dir, arguments[0]);
  } else { // Changing directory forward
    char* path = GetPath(*dir, arguments[0], FileType::Directory);
    if (path == nullptr) {
      Serial.println(F("Unknown Folder"));
      return false;
    }
    free(*dir);
    uint8_t sl = strlen(path);
    if (path[sl - 1] != '/') {
      path[sl] = '/';
      path[sl + 1] = '\0';
    }
    *dir = path;
  }

  Serial.print(F("Changed directory to \""));
  Serial.print(*dir);
  Serial.println(F("\""));
  return false;
}
bool Filebrowser::Jobs::MKDIR(char** dir, uint_least8_t am_arg, char** arguments) {
  if (am_arg < 1) {
    Serial.println(F("No directory name given!"));
    return false;
  }

  char path[strlen(*dir) + strlen(*arguments) + 1];
  strcpy(path, *dir);
  strcat(path, *arguments);

  Serial.print(F("Making directory \""));
  Serial.print(path);
  Serial.println(F("\"..."));

  SD.mkdir(path);
  return false;
}

bool Filebrowser::Jobs::CAT(char** dir, uint_least8_t am_arg, char** arguments) {
  if (am_arg < 1) {
    Serial.println(F("No file/folder given!"));
    return false;
  }

  char* path = GetPath(*dir, arguments[0], FileType::File);
  if (path == nullptr) {
    Serial.println(F("Unknown File"));
    return false;
  }

  Serial.print(F("Reading file \""));
  Serial.print(path);
  Serial.println(F("\":"));
  File file = SD.open(path, FILE_READ);
  while (file.available()) Serial.write(file.read());
  file.close();
  Serial.println();
  free(path);
  return false;
}

bool Filebrowser::Jobs::RM(char** dir, uint_least8_t am_arg, char** arguments) {
  if (am_arg < 1) {
    Serial.println(F("No file/folder given!"));
    return false;
  }

  for (uint_fast8_t i = 0; i < am_arg; i++) {
    char* path = GetPath(*dir, arguments[i], FileType::Both);
    if (path == nullptr) {
      Serial.println(F("Unknown File/Folder"));
      return false;
    }

    File entry = SD.open(path);
    if (entry.isDirectory()) {
      Serial.print(F("Removing Directory \""));
      Serial.print(path);
      Serial.println(F("\"..."));
      File f;
      while (f = entry.openNextFile()) {
        char* fol = (char*)malloc(strlen(f.name()) * sizeof(*fol));
        strcpy(fol, f.name());
        if (Filebrowser::Jobs::RM(&path, 1, &fol)) {
          Serial.println(F("Could not delete directory!"));
          free(fol);
          free(path);
          return false;
        }
        free(fol);
      }
      entry.close();
      SD.rmdir(path);
    } else {
      Serial.print(F("Removing file \""));
      Serial.print(path);
      Serial.println(F("\"..."));
      entry.close();
      SD.remove(path);
    }
    free(path);
  }
  return false;
}

bool Filebrowser::Jobs::PWD(char** dir, uint_least8_t am_arg, char** arguments) {
  Serial.print(F("Current path: \""));
  Serial.print(*dir);
  Serial.println(F("\""));
  return false;
}

char* Filebrowser::GetPath(char* dir, char* file, FileType filetype) {
  uint8_t lastslash = dir[strlen(dir) - 1] == '/' ? 0 : 1;
  char* path = (char*)malloc((strlen(dir) + strlen(file) + 2 + lastslash) * sizeof(path));
  strcpy(path, dir);
  if (lastslash)
    strcat(path, "/");
  strcat(path, file);
  if (SD.exists(path)) {
    if (filetype == FileType::Both)
      return path;

    File f = SD.open(path);
    if ((f.isDirectory() ? FileType::Directory : FileType::File) == filetype) {
      f.close();
      return path;
    } else {
      f.close();
      return nullptr;
    }
  }
  free(path);

  File directory = SD.open(dir);

  uint8_t filelength = strlen(file);
  while (true) {
    File entry = directory.openNextFile();
    if (!entry)
      return nullptr; // no more files

    char* nextfilename = entry.name();
    if (strncmp(file, nextfilename, filelength) == 0) {
      char* path = (char*)malloc((strlen(dir) + strlen(nextfilename) + 1) * sizeof(path));
      strcpy(path, dir);
      strcat(path, nextfilename);
      File f = SD.open(path);
      if (filetype == FileType::Both || (f.isDirectory() ? FileType::Directory : FileType::File) == filetype) {
        f.close();
        entry.close();
        directory.close();
        char* path = (char*)malloc((strlen(dir) + strlen(nextfilename) + 1) * sizeof(path));
        strcpy(path, dir);
        strcat(path, nextfilename);
        return path;
      }
    }
    entry.close();
  }
  directory.close();
  return nullptr;
}