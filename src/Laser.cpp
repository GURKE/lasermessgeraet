#include "Laser.h"
#include <stdlib.h>
#include <Arduino.h>

char answer[6];

/*  Laser_Request - Sendet eine Anfrage (request) über die serielle Schnittstelle an das Lasermessgerät und wertet die Antwort aus
    Paramter:
    request - Die zu sendene Anfrage
    answersize (=2) - Die Größe in Byte des zu erwartenden Antwortstrings (+ das Nullzeichen!)
    timeout (=1000) - Die Zeit, die die Funktion auf eine Antwort warten soll
    Rückgabewert:
    char  * - Der Antwortstring, bei leerer Antwort: "\0"
*/
char* Laser_Request(const char* request, uint8_t answersize, uint32_t timeout) {
  // Clear Bufffer
  while (Serial2.available())
    char c = Serial2.read();

  Serial2.println(request);
  timeout += millis();
  uint8_t i = 0;

  while (timeout > millis() && i < answersize && (i == 0 || answer[i - 1] != '\r')) {
    if (Serial2.available()) {
      answer[i++] = Serial2.read();
      if (i == 1 && (answer[0] == '\n' || answer[0] == '\r')) // Möglichen falsch empfangenen Zeilenumbruch am Anfang löschen
        i--;
    }
  }

  answer[i] = '\0';
  return answer;
}

/*  is_laser_on - Sendet eine Anfrage an das Lasermessgerät, ob das Gerät eingeschaltet ist und gibt die Antwort als bool zurück
    Parameter:
    -
    Rückgabewert:
    bool - Ist wahr, wenn das Gerät eingeschaltet ist
*/
bool is_laser_on() {
  return Laser_Request("p")[0] == '1';
}

/*  laser_activate - Sendet eine Anfrage an das Lasermessgerät, den Laser dauerhaft einzuschalten
    Parameter:
    -
    Rückgabewert:
    -
*/
void laser_activate() {
  Serial2.print("l");
}

/*  laser_deactivate - Sendet eine Anfrage an das Lasermessgerät, den Laser wieder abzuschalten
    Parameter:
    -
    Rückgabewert:
    -
*/
void laser_deactivate() {
  Serial2.print(" ");
}

/**
 * Sendet eine Anfrage an das Lasermessgerät, einen Distanzwert zu messen und zurückzugeben
 * @param response Der Messwert als maximal 6-stelliges Char-Array (inkl. Nullzeichen)
 * @param tries Die Anzahl Versuche, die das Messgerät bekommt, einen plausiblen Messwert zu messen
 * @returns True, wenn Messung erfolgreich
 */
bool manual_measure(char* response, uint8_t tries) {
  char* answer;
  do {
    answer = Laser_Request("m", 5, 10000);
  } while (tries-- && answer[0] == '-');
  answer[4] = '\0';
  strcpy(response, answer);
  return response[0] != '-';
}

/**
 * Sendet eine Anfrage an das Lasermessgerät, einen Distanzwert zu messen und zurückzugeben und gibt diesen als uint16_t zurück
 * @param response Der Messwert als maximal 6-stelliges Char-Array (inkl. Nullzeichen)
 * @param tries Die Anzahl Versuche, die das Messgerät bekommt, einen plausiblen Messwert zu messen
 * @returns True, wenn Messung erfolgreich
 */
bool manual_measure_int(uint16_t* response, uint8_t tries) {
  char res[5];
  bool b = manual_measure(res, tries);
  if (b)
    *response = StringToUInt16_t(res);
  else
    *response = 0;

  return b;
}

/**
 * Sendet tries Anzahl an Anfragen an das Lasermessgerät, einen Distanzwert zu messen und zurückzugeben und mittelt den Wert, bei größerer Abweichung als max_deviation zwischen den Messwerten, gibt die Funktion false zurück
 * @param response Der Messwert als maximal 6-stelliges Char-Array (inkl. Nullzeichen)
 * @param samples Die Anzahl der zu vergleichenden und gemittelten Messwerte
 * @param max_deviation Die maximale Abweichung zwischen den Messwerten, die noch als korrekt gewertet wird
 * @param tries Die Anzahl Messversuche, bis ein Abbruch erfolgt
 * @returns True, wenn Messung erfolgreich
 */
bool manual_measure_averaged(uint16_t* response, uint8_t samples, uint8_t max_deviation, uint8_t tries, bool print_val) {
  uint16_t minimum = 0, maximum = 0;

  uint32_t averaged = 0;
  for (uint8_t i = 0; i < samples; i++) {
    uint16_t value;
    if (!manual_measure_int(&value, tries)) // Kein plausiblen Messwert im Bereich der maximalen Versuche bestimmt
      return false;

    if (value < minimum || !i)
      minimum = value;
    if (value > maximum || !i)
      maximum = value;
    if (minimum + max_deviation < maximum) // Abweichung zwischen Messwerten zu hoch
      return false;
    averaged += value;

    if (print_val) {
      Serial.print(" ");
      Serial.print(value);
    }
  }
  *response = averaged * 1.0 / samples;
  return true;
}


/*  StringToUInt16_t - Wandelt einen eingelesenen Distanzmesswert (char-Array, Wert in mm) in einen uint16_t um und gibt diesen Wert zurück
    Parameter:
    measurement - Der Distanzwert als char-Array in mm
    Rückgabewert:
    uint16_t - Der Distanzwert in mm
*/
uint16_t StringToUInt16_t(char* measurement) {
  uint16_t value = 0;

  for (uint16_t ie = 1, i = strlen(measurement) - 1; i < (uint16_t)-1; i--, ie *= 10)
    value += ie * (measurement[i] - '0');

  return value;
}
