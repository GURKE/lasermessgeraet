#ifndef _LASER_H
#define _LASER_H

#include <stdint.h>

char* Laser_Request(const char* request, uint8_t answersize = 2, uint32_t timeout = 1000);
bool is_laser_on();
void laser_activate();
void laser_deactivate();
bool manual_measure(char* response, uint8_t tries = 1);
bool manual_measure_int(uint16_t* response, uint8_t tries = 1);
bool manual_measure_averaged(uint16_t* response, uint8_t samples = 1, uint8_t max_deviation = 1, uint8_t tries = 5, bool print_val = false);
uint16_t StringToUInt16_t(char* measurement);

#endif