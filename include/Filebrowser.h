#ifndef _FILEBROWSER_H
#define _FILEBROWSER_H

#include "SD.h" // https://www.arduino.cc/en/Tutorial/listfiles
#include <stdint.h>

static const uint_least8_t Max_Filenamelength = 8; // Achtung EEPROM für Standarddateinamen freihalten!
extern char AM_filename[Max_Filenamelength];
static const uint16_t eeprom_AM_filename = 0x80;
static const uint8_t SD_SELECT = 53;

void filebrowser();

class Filebrowser {
public:
  class Jobs {
  public:
    static bool Q(char** dir, uint_least8_t am_arg, char** arguments);
    static bool LS(char** dir, uint_least8_t am_arg, char** arguments);
    static bool CD(char** dir, uint_least8_t am_arg, char** arguments);
    static bool MKDIR(char** dir, uint_least8_t am_arg, char** arguments);
    static bool CAT(char** dir, uint_least8_t am_arg, char** arguments);
    static bool RM(char** dir, uint_least8_t am_arg, char** arguments);
    static bool PWD(char** dir, uint_least8_t am_arg, char** arguments);
  };

private:
  enum class FileType { Directory, File, Both };

public:
  static char* GetPath(char* dir, char* file, FileType filetype = FileType::Both);

};

#endif