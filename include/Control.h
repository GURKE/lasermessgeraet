#ifndef _CONTROL_H
#define _CONTROL_H

#include <stdint.h>
#include <WString.h>
#include "LiquidCrystal.h"

#define SERIALINFOTEXT F("Allgemeine Befehle, die immer ausgesführt werden können:\n(#o)n - Laser dauerhaft einschalten\n(#l)asermessung - Einzelne Entfernungsmessung durchführen\n(#p)unkt - Misst die Distanz und bestimmt die auf das Messobjekt referenzierten R-S-T-Koordinaten\n\nMenüführung:\n(1...9) - Menüauswahl\n(q)-Zurück\n\nWICHTIG: Zeilenende auf 'Neue Zeile' einstellen!\n")
#define log10p(x) (x == 1 ? 4 : (x == 10 ? 3 : (x == 100 ? 2 : (x == 1000 ? 1 : 0))))

extern LiquidCrystal lcd;
/*
    ä = \xE1
    ö = \xEF
    ü = \xF5
    ß = \xE2
*/

#define MESSAGE_TYPE_INFO 0
#define MESSAGE_TYPE_CONFIRMATION 1
#define MESSAGE_TYPE_YESNO 2
#define MESSAGE_TYPE_YESNOBACK 3

#define MESSAGE_RESPONSE_NO 0
#define MESSAGE_RESPONSE_YES 1
#define MESSAGE_RESPONSE_BACK 2


/*  Taster  *****************************************************************************************************************************************************************************************/
#define SwitchNone                  0
#define SwitchRight                 1
#define SwitchTop                   2
#define SwitchBottom                3
#define SwitchLeft                  4
#define SwitchSelect                5
#define SwitchMenu                  6
#define SwitchFilter                0b1111
#define SwitchSourceExternalSwitch  32
#define SwitchSourceFront           64
#define SwitchSourceSerial          128

#define ExternalSwitchPin 15
#define EXTERNALSWITCHCLICKED() (digitalRead(ExternalSwitchPin) == LOW)

extern uint8_t(*_SpecialCommand)(char c);
extern char last_read_char;

/*  Tasterstatus - Gibt zurück, ob und welcher Taster gedrückt wurde. Diese Funktion wertet auch Eingaben über die Serielle Schnittstelle über USB (UART 0) aus.
    Zusätzlich prüft sie, ob ein Taster gedrückt gehalten wird
    Parameter:

    Rückgabewerte:
    uint8_t - Tasterwert (SwitchNone, SwitchBottom, SwitchTop, etc.)

  HINWEIS: Wenn willkürlich Taster gedrückt zu schein werden, kann das an dem Bildschirm liegen, der nicht richtig drauf gedrückt ist. Dann werde random Analog-Taster-Werte gemessen.
*/
uint8_t Tasterstatus();

/*  Tasterstatus_Filtered - Gibt den quellenbefreiten Tasterstatus zurück
    Paramter:
    -
    Rückgabewert:
    -
*/
uint8_t Tasterstatus_Filtered();

/*  lcd_print - Druckt zwei Zeilen auf dem LCD
    Paramter:
      line1 - Text der ersten Zeile (maximal 16 Zeichen)
      line2 - Text der zweiten Zeile (maximal 16 Zeichen)
    Rückgabewert:
      /
*/
void lcd_print(const __FlashStringHelper* line1, const __FlashStringHelper* line2);
void lcd_print(const char* line1, const char* line2);

/*  lcd_print_r - Druckt zwei Zeilen auf dem LCD
    Paramter:
      line1 - Text der ersten Zeile (maximal 16 Zeichen)
      line2 - Text der zweiten Zeile (maximal 16 Zeichen)
    Rückgabewert:
      /
*/
void lcd_print_r(const char* line1, const char* line2);

/*  message - text wird auf LCD angezeigt und kann mit den Tasten durchgescrollt werden, mit select wird die Nachricht beendet
    Parameter:
      lcdText - Der auf dem LCD anzuzeigende Text, Text mit mehr als 16 Zeichen wird als scrollbarer Text angezeigt, Maximal 255 Zeichen möglich
      serialText - Der über die USB-Schnittstelle ausgegebene Text
      message_type (= MESSAGE_TYPE_INFO)- Je nach Typ (MESSAGE_TYPE_INFO, MESSAGE_TYPE_CONFIRMATION, MESSAGE_TYPE_YESNO, MESSAGE_TYPE_YESNOBACK) werden Eingaben erwartet, bevor die Funktion beendet wird.
    Rückgabewert:
      uint8_t - Gibt das Ergebnis einer Abfrage zurück (bei message_type == MESSAGE_TYPE_YESNO oder message_type == MESSAGE_TYPE_YESNOBACK)
*/
uint8_t message(const __FlashStringHelper* lcdText, const __FlashStringHelper* serialText, uint8_t message_type = MESSAGE_TYPE_INFO);

/**
 * Liest eine Zahl ein
 * @params lcdInfotText Der auf dem LCD angezeigte Info-Text in der ersten Zeile (maximal 16 Zeichen)
 * @params serialText Die über die serielle Schnittstelle auszugebende Nachricht
 * @params result Die Adresse, in der das Ergebnis gespeichert wird
 * @params initial_value Der Startwert bei der Eingabe
 * @params maximum Der höchste eingebare Wert
 * @returns Erfolg der Eingabe
 */
bool get_value(const __FlashStringHelper* lcdInfotText, const __FlashStringHelper* serialText, uint16_t* result, uint16_t initial_value = -1, uint16_t maximum = -1);

#endif