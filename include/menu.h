#ifndef MENU_H
#define MENU_H
#include <WString.h>
#include <EEPROM.h>
#include <stdint.h>

class Menu;

extern Menu main_menu;
extern const uint8_t inkrement_index;
extern const uint8_t border_index;
extern const uint8_t automaticmeasure_index;
extern const uint8_t aktuelledaten_index;
extern const uint8_t tests_index;
extern const uint8_t settings_index;
extern const uint8_t settings_steppers_index;
extern const uint8_t streben_index;

enum class Stepper_Part { D, X, Both };
extern bool (*_move_function)(uint32_t* x, const uint16_t eeprom_x, uint16_t* d, const uint16_t eeprom_d, Stepper_Part sp, const __FlashStringHelper* hint);

class Variable {
public:
  enum class Type : uint8_t { UINT16, UINT32, BOOL, SELECTION, POSITION, FLOAT } type;

  const __FlashStringHelper* description;
  void* variable;
  const uint16_t eeprom;

  class T_Select {
  public:
    uint8_t* value;
    const uint8_t amount;
    const char** options;
    T_Select(uint8_t* _value, uint8_t _amount, const char** _options);
  };

  class T_Position {
  public:
    Stepper_Part part;
    uint32_t* X;
    const uint16_t eeprom_X;
    uint16_t* D;
    const uint16_t eeprom_D;
    T_Position(Stepper_Part _part, uint32_t* _X, const uint16_t _eeprom_X, uint16_t* _D = 0, const uint16_t _eeprom_D = 0);
  };

  Variable(Type _type, const __FlashStringHelper* _description, void* _variable, const uint16_t _eeprom = -1);
  void Change();
};

class Menu {
public:
  class Element;
  uint8_t am_elements;
  Menu::Element** elements;
  Element* parent;

  Menu();
  Menu(uint8_t _am_elements, Menu::Element* _elements[]);

  enum class Type : uint8_t { Menu, Variable, Function };


  class Element {
  public:
    const char* title;
    Menu* parent;
    Type type;

    Menu* menu;
    bool(*fun)(bool serial);
    Variable* variable;

    Element();
    Element(const char* _title);
    Element(const char* _title, Menu* _menu);
    Element(const char* _title, Type _type);
    Element(const char* _title, bool(*_fun)(bool serial));
    bool enter(Menu** cur_menu, bool serial);
    bool leave(Menu** cur_menu, bool serial);
  };
};
#endif
