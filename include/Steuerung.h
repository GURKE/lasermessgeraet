#ifndef __STEUERUNG_H
#define __STEUERUNG_H

#include <stdint.h>
#include <menu.h>

// Messungen
void automatic_measure_main(bool serial = true);
bool automatic_measure(bool serial, char** buffer_return, uint_fast16_t& am_points, char* filename_value, char* filename_log);
bool ZOK(uint_fast32_t x, uint_fast16_t d, uint_fast16_t measure);
char* saveCoord(char* line_buffer, int_fast16_t R, float S, float T, uint_fast16_t* am_points, uint_fast16_t line_width, char* line_start);

// Schrittmotor-Funktionen
bool stepperMoveX(int_fast16_t x, Stepper_Part sp = Stepper_Part::Both, bool bordersafe = true, bool waituntilfinished = true);
bool stepperMoveD(int_fast16_t d, Stepper_Part sp = Stepper_Part::Both, bool bordersafe = true, bool waituntilfinished = true);
bool stepperMoveTo(int_fast32_t x, int_fast16_t d, Stepper_Part sp = Stepper_Part::Both, bool bordersafe = true, bool waituntilfinished = true);
void direct_move_control(bool bordersafe = true);
void resetLimitSwitchD();
void resetLimitSwitchX();
void referenceAxis();
void isr_EndSwitchD();
void isr_EndSwitchX();

// Menüfunktionen
void set_inkrement();
void set_filename();
void set_struts_height();
bool Set_MObject_Reference(bool serial = false);
bool set_start_position(bool serial = false);

// Nothalt-Funktionen
void EmergencyStopTriggered_Handle();
void isr_EmergencyStop();

// Stop-Taster-Funktionen
void isr_Stop();
bool AM_Menu_Check(uint8_t index);
bool check_state(uint32_t x);

// Menü-Funktionen
uint8_t SpecialCommand(char c);
bool move_function(uint32_t* x, const uint16_t eeprom_x, uint16_t* d, const uint16_t eeprom_d, Stepper_Part sp, const __FlashStringHelper* hint);
void reboot();

// Test-Funktionen
void X_Axis_test();
void D_Axis_test();
void Length_test();
void Precision_test();

#endif